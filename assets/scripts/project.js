'use strict';

/**
 * Show/Hide the Search Form in the header.
 *
 * @author Corey Collins
 */
window.ShowHideSearchForm = {};
(function (window, $, app) {

	// Constructor
	app.init = function () {
		app.cache();

		if (app.meetsRequirements()) {
			app.bindEvents();
		}
	};

	// Cache all the things
	app.cache = function () {
		app.$c = {
			window: $(window),
			body: $('body'),
			headerSearchForm: $('.site-header-action .cta-button')
		};
	};

	// Combine all events
	app.bindEvents = function () {
		app.$c.headerSearchForm.on('keyup touchstart click', app.showHideSearchForm);
		app.$c.body.on('keyup touchstart click', app.hideSearchForm);
	};

	// Do we meet the requirements?
	app.meetsRequirements = function () {
		return app.$c.headerSearchForm.length;
	};

	// Adds the toggle class for the search form.
	app.showHideSearchForm = function () {
		app.$c.body.toggleClass('search-form-visible');
	};

	// Hides the search form if we click outside of its container.
	app.hideSearchForm = function (event) {

		if (!$(event.target).parents('div').hasClass('site-header-action')) {
			app.$c.body.removeClass('search-form-visible');
		}
	};

	// Engage
	$(app.init);
})(window, jQuery, window.ShowHideSearchForm);
'use strict';

/**
 * File hero-carousel.js
 *
 * Create a carousel if we have more than one hero slide.
 */
window.wdsHeroCarousel = {};
(function (window, $, app) {

	// Constructor.
	app.init = function () {
		app.cache();

		if (app.meetsRequirements()) {
			app.bindEvents();
		}
	};

	// Cache all the things.
	app.cache = function () {
		app.$c = {
			window: $(window),
			heroCarousel: $('.carousel')
		};
	};

	// Combine all events.
	app.bindEvents = function () {
		app.$c.window.on('load', app.doSlick);
		app.$c.window.on('load', app.doFirstAnimation);
	};

	// Do we meet the requirements?
	app.meetsRequirements = function () {
		return app.$c.heroCarousel.length;
	};

	// Animate the first slide on window load.
	app.doFirstAnimation = function () {

		// Get the first slide content area and animation attribute.
		var firstSlide = app.$c.heroCarousel.find('[data-slick-index=0]'),
		    firstSlideContent = firstSlide.find('.hero-content'),
		    firstAnimation = firstSlideContent.attr('data-animation');

		// Add the animation class to the first slide.
		firstSlideContent.addClass(firstAnimation);
	};

	// Animate the slide content.
	app.doAnimation = function () {
		var slides = $('.slide'),
		    activeSlide = $('.slick-current'),
		    activeContent = activeSlide.find('.hero-content'),


		// This is a string like so: 'animated someCssClass'.
		animationClass = activeContent.attr('data-animation'),
		    splitAnimation = animationClass.split(' '),


		// This is the 'animated' class.
		animationTrigger = splitAnimation[0];

		// Go through each slide to see if we've already set animation classes.
		slides.each(function () {
			var slideContent = $(this).find('.hero-content');

			// If we've set animation classes on a slide, remove them.
			if (slideContent.hasClass('animated')) {

				// Get the last class, which is the animate.css class.
				var lastClass = slideContent.attr('class').split(' ').pop();

				// Remove both animation classes.
				slideContent.removeClass(lastClass).removeClass(animationTrigger);
			}
		});

		// Add animation classes after slide is in view.
		activeContent.addClass(animationClass);
	};

	// Allow background videos to autoplay.
	app.playBackgroundVideos = function () {

		// Get all the videos in our slides object.
		$('video').each(function () {

			// Let them autoplay. TODO: Possibly change this later to only play the visible slide video.
			this.play();
		});
	};

	// Kick off Slick.
	app.doSlick = function () {
		app.$c.heroCarousel.on('init', app.playBackgroundVideos);

		app.$c.heroCarousel.slick({
			autoplay: true,
			autoplaySpeed: 5000,
			arrows: false,
			dots: false,
			focusOnSelect: true,
			waitForAnimate: true
		});

		app.$c.heroCarousel.on('afterChange', app.doAnimation);
	};

	// Engage!
	$(app.init);
})(window, jQuery, window.wdsHeroCarousel);
'use strict';

/**
 * File js-enabled.js
 *
 * If Javascript is enabled, replace the <body> class "no-js".
 */
document.body.className = document.body.className.replace('no-js', 'js');
'use strict';

/**
 * File: mobile-menu.js
 *
 * Create an accordion style dropdown.
 */
window.wdsMobileMenu = {};
(function (window, $, app) {

	// Constructor.
	app.init = function () {
		app.cache();

		if (app.meetsRequirements()) {
			app.bindEvents();
		}
	};

	// Cache all the things.
	app.cache = function () {
		app.$c = {
			body: $('body'),
			window: $(window),
			subMenuContainer: $('.mobile-menu .sub-menu, .utility-navigation .sub-menu'),
			subSubMenuContainer: $('.mobile-menu .sub-menu .sub-menu'),
			subMenuParentItem: $('.mobile-menu li.menu-item-has-children, .utility-navigation li.menu-item-has-children'),
			offCanvasContainer: $('.off-canvas-container')
		};
	};

	// Combine all events.
	app.bindEvents = function () {
		app.$c.window.on('load', app.addDownArrow);
		app.$c.subMenuParentItem.on('click', app.toggleSubmenu);
		app.$c.subMenuParentItem.on('transitionend', app.resetSubMenu);
		app.$c.offCanvasContainer.on('transitionend', app.forceCloseSubmenus);
	};

	// Do we meet the requirements?
	app.meetsRequirements = function () {
		return app.$c.subMenuContainer.length;
	};

	// Reset the submenus after it's done closing.
	app.resetSubMenu = function () {

		// When the list item is done transitioning in height,
		// remove the classes from the submenu so it is ready to toggle again.
		if ($(this).is('li.menu-item-has-children') && !$(this).hasClass('is-visible')) {
			$(this).find('ul.sub-menu').removeClass('slideOutLeft is-visible');
		}
	};

	// Slide out the submenu items.
	app.slideOutSubMenus = function (el) {

		// If this item's parent is visible and this is not, bail.
		if (el.parent().hasClass('is-visible') && !el.hasClass('is-visible')) {
			return;
		}

		// If this item's parent is visible and this item is visible, hide its submenu then bail.
		if (el.parent().hasClass('is-visible') && el.hasClass('is-visible')) {
			el.removeClass('is-visible').find('.sub-menu').removeClass('slideInLeft').addClass('slideOutLeft');
			return;
		}

		app.$c.subMenuContainer.each(function () {

			// Only try to close submenus that are actually open.
			if ($(this).hasClass('slideInLeft')) {

				// Close the parent list item, and set the corresponding button aria to false.
				$(this).parent().removeClass('is-visible').find('.parent-indicator').attr('aria-expanded', false);

				// Slide out the submenu.
				$(this).removeClass('slideInLeft').addClass('slideOutLeft');
			}
		});
	};

	// Add the down arrow to submenu parents.
	app.addDownArrow = function () {
		app.$c.subMenuParentItem.prepend('<button type="button" aria-expanded="false" class="parent-indicator" aria-label="Open submenu"><span class="down-arrow"></span></button>');
	};

	// Deal with the submenu.
	app.toggleSubmenu = function (e) {

		var el = $(this),
		    // The menu element which was clicked on.
		subMenu = el.children('ul.sub-menu'),
		    // The nearest submenu.
		$target = $(e.target); // the element that's actually being clicked (child of the li that triggered the click event).

		// Figure out if we're clicking the button or its arrow child,
		// if so, we can just open or close the menu and bail.
		if ($target.hasClass('down-arrow') || $target.hasClass('parent-indicator')) {

			// First, collapse any already opened submenus.
			app.slideOutSubMenus(el);

			if (!subMenu.hasClass('is-visible')) {

				// Open the submenu.
				app.openSubmenu(el, subMenu);
			}

			return false;
		}
	};

	// Open a submenu.
	app.openSubmenu = function (parent, subMenu) {

		// Expand the list menu item, and set the corresponding button aria to true.
		parent.addClass('is-visible').find('.parent-indicator').attr('aria-expanded', true);

		// Slide the menu in.
		subMenu.addClass('is-visible animated slideInLeft');
	};

	// Force close all the submenus when the main menu container is closed.
	app.forceCloseSubmenus = function () {

		// The transitionend event triggers on open and on close, need to make sure we only do this on close.
		if (!$(this).hasClass('is-visible')) {
			app.$c.subMenuParentItem.removeClass('is-visible').find('.parent-indicator').attr('aria-expanded', false);
			app.$c.subMenuContainer.removeClass('is-visible slideInLeft');
			app.$c.body.css('overflow', 'visible');
			app.$c.body.unbind('touchstart');
		}

		if ($(this).hasClass('is-visible')) {
			app.$c.body.css('overflow', 'hidden');
			app.$c.body.bind('touchstart', function (e) {
				if (!$(e.target).parents('.contact-modal')[0]) {
					e.preventDefault();
				}
			});
		}
	};

	// Engage!
	$(app.init);
})(window, jQuery, window.wdsMobileMenu);
'use strict';

/**
 * File modal.js
 *
 * Deal with multiple modals and their media.
 */
window.wdsModal = {};
(function (window, $, app) {

	var $modalToggle = void 0,
	    $focusableChildren = void 0,
	    $player = void 0,
	    $tag = document.createElement('script'),
	    $firstScriptTag = document.getElementsByTagName('script')[0],
	    YT = void 0;

	// Constructor.
	app.init = function () {
		app.cache();

		if (app.meetsRequirements()) {
			$firstScriptTag.parentNode.insertBefore($tag, $firstScriptTag);
			app.bindEvents();
		}
	};

	// Cache all the things.
	app.cache = function () {
		app.$c = {
			'body': $('body')
		};
	};

	// Do we meet the requirements?
	app.meetsRequirements = function () {
		return $('.modal-trigger').length;
	};

	// Combine all events.
	app.bindEvents = function () {

		// Trigger a modal to open.
		app.$c.body.on('click touchstart', '.modal-trigger', app.openModal);

		// Trigger the close button to close the modal.
		app.$c.body.on('click touchstart', '.close', app.closeModal);

		// Allow the user to close the modal by hitting the esc key.
		app.$c.body.on('keydown', app.escKeyClose);

		// Allow the user to close the modal by clicking outside of the modal.
		app.$c.body.on('click touchstart', 'div.modal-open', app.closeModalByClick);

		// Listen to tabs, trap keyboard if we need to
		app.$c.body.on('keydown', app.trapKeyboardMaybe);
	};

	// Open the modal.
	app.openModal = function () {

		// Store the modal toggle element
		$modalToggle = $(this);

		// Figure out which modal we're opening and store the object.
		var $modal = $($(this).data('target'));

		// Display the modal.
		$modal.addClass('modal-open');

		// Add body class.
		app.$c.body.addClass('modal-open');

		// Find the focusable children of the modal.
		// This list may be incomplete, really wish jQuery had the :focusable pseudo like jQuery UI does.
		// For more about :input see: https://api.jquery.com/input-selector/
		$focusableChildren = $modal.find('a, :input, [tabindex]');

		// Ideally, there is always one (the close button), but you never know.
		if (0 < $focusableChildren.length) {

			// Shift focus to the first focusable element.
			$focusableChildren[0].focus();
		}
	};

	// Close the modal.
	app.closeModal = function () {

		// Figure the opened modal we're closing and store the object.
		var $modal = $($('div.modal-open .close').data('target')),


		// Find the iframe in the $modal object.
		$iframe = $modal.find('iframe');

		// Only do this if there are any iframes.
		if ($iframe.length) {

			// Get the iframe src URL.
			var url = $iframe.attr('src');

			// Removing/Readding the URL will effectively break the YouTube API.
			// So let's not do that when the iframe URL contains the enablejsapi parameter.
			if (!url.includes('enablejsapi=1')) {

				// Remove the source URL, then add it back, so the video can be played again later.
				$iframe.attr('src', '').attr('src', url);
			} else {

				// Use the YouTube API to stop the video.
				$player.stopVideo();
			}
		}

		// Finally, hide the modal.
		$modal.removeClass('modal-open');

		// Remove the body class.
		app.$c.body.removeClass('modal-open');

		// Revert focus back to toggle element
		$modalToggle.focus();
	};

	// Close if "esc" key is pressed.
	app.escKeyClose = function (event) {
		if (27 === event.keyCode) {
			app.closeModal();
		}
	};

	// Close if the user clicks outside of the modal
	app.closeModalByClick = function (event) {

		// If the parent container is NOT the modal dialog container, close the modal
		if (!$(event.target).parents('div').hasClass('modal-dialog')) {
			app.closeModal();
		}
	};

	// Trap the keyboard into a modal when one is active.
	app.trapKeyboardMaybe = function (event) {

		// We only need to do stuff when the modal is open and tab is pressed.
		if (9 === event.which && 0 < $('.modal-open').length) {
			var $focused = $(':focus'),
			    focusIndex = $focusableChildren.index($focused);

			if (0 === focusIndex && event.shiftKey) {

				// If this is the first focusable element, and shift is held when pressing tab, go back to last focusable element.
				$focusableChildren[$focusableChildren.length - 1].focus();
				event.preventDefault();
			} else if (!event.shiftKey && focusIndex === $focusableChildren.length - 1) {

				// If this is the last focusable element, and shift is not held, go back to the first focusable element.
				$focusableChildren[0].focus();
				event.preventDefault();
			}
		}
	};

	// Hook into YouTube <iframe>.
	app.onYouTubeIframeAPIReady = function () {
		var $modal = $('div.modal'),
		    $iframeid = $modal.find('iframe').attr('id');

		$player = new YT.Player($iframeid, {
			events: {
				'onReady': app.onPlayerReady,
				'onStateChange': app.onPlayerStateChange
			}
		});
	};

	// Do something on player ready.
	app.onPlayerReady = function () {};

	// Do something on player state change.
	app.onPlayerStateChange = function () {

		// Set focus to the first focusable element inside of the modal the player is in.
		$(event.target.a).parents('.modal').find('a, :input, [tabindex]').first().focus();
	};

	// Engage!
	$(app.init);
})(window, jQuery, window.wdsModal);
'use strict';

/**
 * File: navigation-primary.js
 *
 * Helpers for the primary navigation.
 */
window.wdsPrimaryNavigation = {};
(function (window, $, app) {

	// Constructor.
	app.init = function () {
		app.cache();

		if (app.meetsRequirements()) {
			app.bindEvents();
		}
	};

	// Cache all the things.
	app.cache = function () {
		app.$c = {
			window: $(window),
			subMenuContainer: $('.main-navigation .sub-menu'),
			subMenuParentItem: $('.main-navigation li.menu-item-has-children')
		};
	};

	// Combine all events.
	app.bindEvents = function () {
		app.$c.window.on('load', app.addDownArrow);
		app.$c.subMenuParentItem.find('a').on('focusin focusout', app.toggleFocus);
	};

	// Do we meet the requirements?
	app.meetsRequirements = function () {
		return app.$c.subMenuContainer.length;
	};

	// Add the down arrow to submenu parents.
	app.addDownArrow = function () {
		app.$c.subMenuParentItem.find('> a').append('<span class="caret-down" aria-hidden="true"></span>');
	};

	// Toggle the focus class on the link parent.
	app.toggleFocus = function () {
		$(this).parents('li.menu-item-has-children').toggleClass('focus');
	};

	// Engage!
	$(app.init);
})(window, jQuery, window.wdsPrimaryNavigation);
'use strict';

/**
 * File: off-canvas.js
 *
 * Help deal with the off-canvas mobile menu.
 */
window.wdsoffCanvas = {};
(function (window, $, app) {

	// Constructor.
	app.init = function () {
		app.cache();

		if (app.meetsRequirements()) {
			app.bindEvents();
		}
	};

	// Cache all the things.
	app.cache = function () {
		app.$c = {
			body: $('body'),
			offCanvasClose: $('.off-canvas-close'),
			offCanvasContainer: $('.off-canvas-container'),
			offCanvasOpen: $('.off-canvas-open'),
			offCanvasScreen: $('.off-canvas-screen')
		};
	};

	// Combine all events.
	app.bindEvents = function () {
		app.$c.body.on('keydown', app.escKeyClose);
		app.$c.offCanvasClose.on('click', app.closeoffCanvas);
		app.$c.offCanvasOpen.on('click', app.toggleoffCanvas);
		app.$c.offCanvasScreen.on('click', app.closeoffCanvas);
	};

	// Do we meet the requirements?
	app.meetsRequirements = function () {
		return app.$c.offCanvasContainer.length;
	};

	// To show or not to show?
	app.toggleoffCanvas = function () {

		if ('true' === $(this).attr('aria-expanded')) {
			app.closeoffCanvas();
		} else {
			app.openoffCanvas();
		}
	};

	// Show that drawer!
	app.openoffCanvas = function () {
		app.$c.offCanvasContainer.addClass('is-visible');
		app.$c.offCanvasOpen.addClass('is-visible');
		app.$c.offCanvasScreen.addClass('is-visible');

		app.$c.offCanvasOpen.attr('aria-expanded', true);
		app.$c.offCanvasContainer.attr('aria-hidden', false);

		app.$c.offCanvasContainer.find('button').first().focus();
	};

	// Close that drawer!
	app.closeoffCanvas = function () {
		app.$c.offCanvasContainer.removeClass('is-visible');
		app.$c.offCanvasOpen.removeClass('is-visible');
		app.$c.offCanvasScreen.removeClass('is-visible');

		app.$c.offCanvasOpen.attr('aria-expanded', false);
		app.$c.offCanvasContainer.attr('aria-hidden', true);

		app.$c.offCanvasOpen.focus();
	};

	// Close drawer if "esc" key is pressed.
	app.escKeyClose = function (event) {
		if (27 === event.keyCode) {
			app.closeoffCanvas();
		}
	};

	// Engage!
	$(app.init);
})(window, jQuery, window.wdsoffCanvas);
'use strict';

/**
 * File skip-link-focus-fix.js.
 *
 * Helps with accessibility for keyboard only users.
 *
 * Learn more: https://git.io/vWdr2
 */
(function () {
	var isWebkit = -1 < navigator.userAgent.toLowerCase().indexOf('webkit'),
	    isOpera = -1 < navigator.userAgent.toLowerCase().indexOf('opera'),
	    isIe = -1 < navigator.userAgent.toLowerCase().indexOf('msie');

	if ((isWebkit || isOpera || isIe) && document.getElementById && window.addEventListener) {
		window.addEventListener('hashchange', function () {
			var id = location.hash.substring(1),
			    element;

			if (!/^[A-z0-9_-]+$/.test(id)) {
				return;
			}

			element = document.getElementById(id);

			if (element) {
				if (!/^(?:a|select|input|button|textarea)$/i.test(element.tagName)) {
					element.tabIndex = -1;
				}

				element.focus();
			}
		}, false);
	}
})();
'use strict';

/**
 * File window-ready.js
 *
 * Add a "ready" class to <body> when window is ready.
 */
window.wdsWindowReady = {};
(function (window, $, app) {

	// Constructor.
	app.init = function () {
		app.cache();
		app.bindEvents();
	};

	// Cache document elements.
	app.cache = function () {
		app.$c = {
			'window': $(window),
			'body': $(document.body)
		};
	};

	// Combine all events.
	app.bindEvents = function () {
		app.$c.window.load(app.addBodyClass);
	};

	// Add a class to <body>.
	app.addBodyClass = function () {
		app.$c.body.addClass('ready');
	};

	// Engage!
	$(app.init);
})(window, jQuery, window.wdsWindowReady);
//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImhlYWRlci1idXR0b24uanMiLCJoZXJvLWNhcm91c2VsLmpzIiwianMtZW5hYmxlZC5qcyIsIm1vYmlsZS1tZW51LmpzIiwibW9kYWwuanMiLCJuYXZpZ2F0aW9uLXByaW1hcnkuanMiLCJvZmYtY2FudmFzLmpzIiwic2tpcC1saW5rLWZvY3VzLWZpeC5qcyIsIndpbmRvdy1yZWFkeS5qcyJdLCJuYW1lcyI6WyJ3aW5kb3ciLCJTaG93SGlkZVNlYXJjaEZvcm0iLCIkIiwiYXBwIiwiaW5pdCIsImNhY2hlIiwibWVldHNSZXF1aXJlbWVudHMiLCJiaW5kRXZlbnRzIiwiJGMiLCJib2R5IiwiaGVhZGVyU2VhcmNoRm9ybSIsIm9uIiwic2hvd0hpZGVTZWFyY2hGb3JtIiwiaGlkZVNlYXJjaEZvcm0iLCJsZW5ndGgiLCJ0b2dnbGVDbGFzcyIsImV2ZW50IiwidGFyZ2V0IiwicGFyZW50cyIsImhhc0NsYXNzIiwicmVtb3ZlQ2xhc3MiLCJqUXVlcnkiLCJ3ZHNIZXJvQ2Fyb3VzZWwiLCJoZXJvQ2Fyb3VzZWwiLCJkb1NsaWNrIiwiZG9GaXJzdEFuaW1hdGlvbiIsImZpcnN0U2xpZGUiLCJmaW5kIiwiZmlyc3RTbGlkZUNvbnRlbnQiLCJmaXJzdEFuaW1hdGlvbiIsImF0dHIiLCJhZGRDbGFzcyIsImRvQW5pbWF0aW9uIiwic2xpZGVzIiwiYWN0aXZlU2xpZGUiLCJhY3RpdmVDb250ZW50IiwiYW5pbWF0aW9uQ2xhc3MiLCJzcGxpdEFuaW1hdGlvbiIsInNwbGl0IiwiYW5pbWF0aW9uVHJpZ2dlciIsImVhY2giLCJzbGlkZUNvbnRlbnQiLCJsYXN0Q2xhc3MiLCJwb3AiLCJwbGF5QmFja2dyb3VuZFZpZGVvcyIsInBsYXkiLCJzbGljayIsImF1dG9wbGF5IiwiYXV0b3BsYXlTcGVlZCIsImFycm93cyIsImRvdHMiLCJmb2N1c09uU2VsZWN0Iiwid2FpdEZvckFuaW1hdGUiLCJkb2N1bWVudCIsImNsYXNzTmFtZSIsInJlcGxhY2UiLCJ3ZHNNb2JpbGVNZW51Iiwic3ViTWVudUNvbnRhaW5lciIsInN1YlN1Yk1lbnVDb250YWluZXIiLCJzdWJNZW51UGFyZW50SXRlbSIsIm9mZkNhbnZhc0NvbnRhaW5lciIsImFkZERvd25BcnJvdyIsInRvZ2dsZVN1Ym1lbnUiLCJyZXNldFN1Yk1lbnUiLCJmb3JjZUNsb3NlU3VibWVudXMiLCJpcyIsInNsaWRlT3V0U3ViTWVudXMiLCJlbCIsInBhcmVudCIsInByZXBlbmQiLCJlIiwic3ViTWVudSIsImNoaWxkcmVuIiwiJHRhcmdldCIsIm9wZW5TdWJtZW51IiwiY3NzIiwidW5iaW5kIiwiYmluZCIsInByZXZlbnREZWZhdWx0Iiwid2RzTW9kYWwiLCIkbW9kYWxUb2dnbGUiLCIkZm9jdXNhYmxlQ2hpbGRyZW4iLCIkcGxheWVyIiwiJHRhZyIsImNyZWF0ZUVsZW1lbnQiLCIkZmlyc3RTY3JpcHRUYWciLCJnZXRFbGVtZW50c0J5VGFnTmFtZSIsIllUIiwicGFyZW50Tm9kZSIsImluc2VydEJlZm9yZSIsIm9wZW5Nb2RhbCIsImNsb3NlTW9kYWwiLCJlc2NLZXlDbG9zZSIsImNsb3NlTW9kYWxCeUNsaWNrIiwidHJhcEtleWJvYXJkTWF5YmUiLCIkbW9kYWwiLCJkYXRhIiwiZm9jdXMiLCIkaWZyYW1lIiwidXJsIiwiaW5jbHVkZXMiLCJzdG9wVmlkZW8iLCJrZXlDb2RlIiwid2hpY2giLCIkZm9jdXNlZCIsImZvY3VzSW5kZXgiLCJpbmRleCIsInNoaWZ0S2V5Iiwib25Zb3VUdWJlSWZyYW1lQVBJUmVhZHkiLCIkaWZyYW1laWQiLCJQbGF5ZXIiLCJldmVudHMiLCJvblBsYXllclJlYWR5Iiwib25QbGF5ZXJTdGF0ZUNoYW5nZSIsImEiLCJmaXJzdCIsIndkc1ByaW1hcnlOYXZpZ2F0aW9uIiwidG9nZ2xlRm9jdXMiLCJhcHBlbmQiLCJ3ZHNvZmZDYW52YXMiLCJvZmZDYW52YXNDbG9zZSIsIm9mZkNhbnZhc09wZW4iLCJvZmZDYW52YXNTY3JlZW4iLCJjbG9zZW9mZkNhbnZhcyIsInRvZ2dsZW9mZkNhbnZhcyIsIm9wZW5vZmZDYW52YXMiLCJpc1dlYmtpdCIsIm5hdmlnYXRvciIsInVzZXJBZ2VudCIsInRvTG93ZXJDYXNlIiwiaW5kZXhPZiIsImlzT3BlcmEiLCJpc0llIiwiZ2V0RWxlbWVudEJ5SWQiLCJhZGRFdmVudExpc3RlbmVyIiwiaWQiLCJsb2NhdGlvbiIsImhhc2giLCJzdWJzdHJpbmciLCJlbGVtZW50IiwidGVzdCIsInRhZ05hbWUiLCJ0YWJJbmRleCIsIndkc1dpbmRvd1JlYWR5IiwibG9hZCIsImFkZEJvZHlDbGFzcyJdLCJtYXBwaW5ncyI6Ijs7QUFBQTs7Ozs7QUFLQUEsT0FBT0Msa0JBQVAsR0FBNEIsRUFBNUI7QUFDQyxXQUFVRCxNQUFWLEVBQWtCRSxDQUFsQixFQUFxQkMsR0FBckIsRUFBMEI7O0FBRTFCO0FBQ0FBLEtBQUlDLElBQUosR0FBVyxZQUFZO0FBQ3RCRCxNQUFJRSxLQUFKOztBQUVBLE1BQUlGLElBQUlHLGlCQUFKLEVBQUosRUFBNkI7QUFDNUJILE9BQUlJLFVBQUo7QUFDQTtBQUNELEVBTkQ7O0FBUUE7QUFDQUosS0FBSUUsS0FBSixHQUFZLFlBQVk7QUFDdkJGLE1BQUlLLEVBQUosR0FBUztBQUNSUixXQUFRRSxFQUFFRixNQUFGLENBREE7QUFFUlMsU0FBTVAsRUFBRSxNQUFGLENBRkU7QUFHUlEscUJBQWtCUixFQUFFLGlDQUFGO0FBSFYsR0FBVDtBQUtBLEVBTkQ7O0FBUUE7QUFDQUMsS0FBSUksVUFBSixHQUFpQixZQUFZO0FBQzVCSixNQUFJSyxFQUFKLENBQU9FLGdCQUFQLENBQXdCQyxFQUF4QixDQUEyQix3QkFBM0IsRUFBcURSLElBQUlTLGtCQUF6RDtBQUNBVCxNQUFJSyxFQUFKLENBQU9DLElBQVAsQ0FBWUUsRUFBWixDQUFlLHdCQUFmLEVBQXlDUixJQUFJVSxjQUE3QztBQUNBLEVBSEQ7O0FBS0E7QUFDQVYsS0FBSUcsaUJBQUosR0FBd0IsWUFBWTtBQUNuQyxTQUFPSCxJQUFJSyxFQUFKLENBQU9FLGdCQUFQLENBQXdCSSxNQUEvQjtBQUNBLEVBRkQ7O0FBSUE7QUFDQVgsS0FBSVMsa0JBQUosR0FBeUIsWUFBWTtBQUNwQ1QsTUFBSUssRUFBSixDQUFPQyxJQUFQLENBQVlNLFdBQVosQ0FBd0IscUJBQXhCO0FBQ0EsRUFGRDs7QUFJQTtBQUNBWixLQUFJVSxjQUFKLEdBQXFCLFVBQVVHLEtBQVYsRUFBaUI7O0FBRXJDLE1BQUksQ0FBQ2QsRUFBRWMsTUFBTUMsTUFBUixFQUFnQkMsT0FBaEIsQ0FBd0IsS0FBeEIsRUFBK0JDLFFBQS9CLENBQXdDLG9CQUF4QyxDQUFMLEVBQW9FO0FBQ25FaEIsT0FBSUssRUFBSixDQUFPQyxJQUFQLENBQVlXLFdBQVosQ0FBd0IscUJBQXhCO0FBQ0E7QUFDRCxFQUxEOztBQU9BO0FBQ0FsQixHQUFFQyxJQUFJQyxJQUFOO0FBRUEsQ0EvQ0EsRUErQ0NKLE1BL0NELEVBK0NTcUIsTUEvQ1QsRUErQ2lCckIsT0FBT0Msa0JBL0N4QixDQUFEOzs7QUNOQTs7Ozs7QUFLQUQsT0FBT3NCLGVBQVAsR0FBeUIsRUFBekI7QUFDQyxXQUFVdEIsTUFBVixFQUFrQkUsQ0FBbEIsRUFBcUJDLEdBQXJCLEVBQTBCOztBQUUxQjtBQUNBQSxLQUFJQyxJQUFKLEdBQVcsWUFBWTtBQUN0QkQsTUFBSUUsS0FBSjs7QUFFQSxNQUFJRixJQUFJRyxpQkFBSixFQUFKLEVBQTZCO0FBQzVCSCxPQUFJSSxVQUFKO0FBQ0E7QUFDRCxFQU5EOztBQVFBO0FBQ0FKLEtBQUlFLEtBQUosR0FBWSxZQUFZO0FBQ3ZCRixNQUFJSyxFQUFKLEdBQVM7QUFDUlIsV0FBUUUsRUFBRUYsTUFBRixDQURBO0FBRVJ1QixpQkFBY3JCLEVBQUUsV0FBRjtBQUZOLEdBQVQ7QUFJQSxFQUxEOztBQU9BO0FBQ0FDLEtBQUlJLFVBQUosR0FBaUIsWUFBWTtBQUM1QkosTUFBSUssRUFBSixDQUFPUixNQUFQLENBQWNXLEVBQWQsQ0FBaUIsTUFBakIsRUFBeUJSLElBQUlxQixPQUE3QjtBQUNBckIsTUFBSUssRUFBSixDQUFPUixNQUFQLENBQWNXLEVBQWQsQ0FBaUIsTUFBakIsRUFBeUJSLElBQUlzQixnQkFBN0I7QUFDQSxFQUhEOztBQUtBO0FBQ0F0QixLQUFJRyxpQkFBSixHQUF3QixZQUFZO0FBQ25DLFNBQU9ILElBQUlLLEVBQUosQ0FBT2UsWUFBUCxDQUFvQlQsTUFBM0I7QUFDQSxFQUZEOztBQUlBO0FBQ0FYLEtBQUlzQixnQkFBSixHQUF1QixZQUFZOztBQUVsQztBQUNBLE1BQUlDLGFBQWF2QixJQUFJSyxFQUFKLENBQU9lLFlBQVAsQ0FBb0JJLElBQXBCLENBQXlCLHNCQUF6QixDQUFqQjtBQUFBLE1BQ0NDLG9CQUFvQkYsV0FBV0MsSUFBWCxDQUFnQixlQUFoQixDQURyQjtBQUFBLE1BRUNFLGlCQUFpQkQsa0JBQWtCRSxJQUFsQixDQUF1QixnQkFBdkIsQ0FGbEI7O0FBSUE7QUFDQUYsb0JBQWtCRyxRQUFsQixDQUEyQkYsY0FBM0I7QUFDQSxFQVREOztBQVdBO0FBQ0ExQixLQUFJNkIsV0FBSixHQUFrQixZQUFZO0FBQzdCLE1BQUlDLFNBQVMvQixFQUFFLFFBQUYsQ0FBYjtBQUFBLE1BQ0NnQyxjQUFjaEMsRUFBRSxnQkFBRixDQURmO0FBQUEsTUFFQ2lDLGdCQUFnQkQsWUFBWVAsSUFBWixDQUFpQixlQUFqQixDQUZqQjs7O0FBSUM7QUFDQVMsbUJBQWlCRCxjQUFjTCxJQUFkLENBQW1CLGdCQUFuQixDQUxsQjtBQUFBLE1BTUNPLGlCQUFpQkQsZUFBZUUsS0FBZixDQUFxQixHQUFyQixDQU5sQjs7O0FBUUM7QUFDQUMscUJBQW1CRixlQUFlLENBQWYsQ0FUcEI7O0FBV0E7QUFDQUosU0FBT08sSUFBUCxDQUFZLFlBQVk7QUFDdkIsT0FBSUMsZUFBZXZDLEVBQUUsSUFBRixFQUFReUIsSUFBUixDQUFhLGVBQWIsQ0FBbkI7O0FBRUE7QUFDQSxPQUFJYyxhQUFhdEIsUUFBYixDQUFzQixVQUF0QixDQUFKLEVBQXVDOztBQUV0QztBQUNBLFFBQUl1QixZQUFZRCxhQUNkWCxJQURjLENBQ1QsT0FEUyxFQUVkUSxLQUZjLENBRVIsR0FGUSxFQUdkSyxHQUhjLEVBQWhCOztBQUtBO0FBQ0FGLGlCQUFhckIsV0FBYixDQUF5QnNCLFNBQXpCLEVBQW9DdEIsV0FBcEMsQ0FBZ0RtQixnQkFBaEQ7QUFDQTtBQUNELEdBZkQ7O0FBaUJBO0FBQ0FKLGdCQUFjSixRQUFkLENBQXVCSyxjQUF2QjtBQUNBLEVBaENEOztBQWtDQTtBQUNBakMsS0FBSXlDLG9CQUFKLEdBQTJCLFlBQVk7O0FBRXRDO0FBQ0ExQyxJQUFFLE9BQUYsRUFBV3NDLElBQVgsQ0FBZ0IsWUFBWTs7QUFFM0I7QUFDQSxRQUFLSyxJQUFMO0FBQ0EsR0FKRDtBQUtBLEVBUkQ7O0FBVUE7QUFDQTFDLEtBQUlxQixPQUFKLEdBQWMsWUFBWTtBQUN6QnJCLE1BQUlLLEVBQUosQ0FBT2UsWUFBUCxDQUFvQlosRUFBcEIsQ0FBdUIsTUFBdkIsRUFBK0JSLElBQUl5QyxvQkFBbkM7O0FBRUF6QyxNQUFJSyxFQUFKLENBQU9lLFlBQVAsQ0FBb0J1QixLQUFwQixDQUEwQjtBQUN6QkMsYUFBVSxJQURlO0FBRXpCQyxrQkFBZSxJQUZVO0FBR3pCQyxXQUFRLEtBSGlCO0FBSXpCQyxTQUFNLEtBSm1CO0FBS3pCQyxrQkFBZSxJQUxVO0FBTXpCQyxtQkFBZ0I7QUFOUyxHQUExQjs7QUFTQWpELE1BQUlLLEVBQUosQ0FBT2UsWUFBUCxDQUFvQlosRUFBcEIsQ0FBdUIsYUFBdkIsRUFBc0NSLElBQUk2QixXQUExQztBQUNBLEVBYkQ7O0FBZUE7QUFDQTlCLEdBQUVDLElBQUlDLElBQU47QUFDQSxDQTFHQSxFQTBHQ0osTUExR0QsRUEwR1NxQixNQTFHVCxFQTBHaUJyQixPQUFPc0IsZUExR3hCLENBQUQ7OztBQ05BOzs7OztBQUtBK0IsU0FBUzVDLElBQVQsQ0FBYzZDLFNBQWQsR0FBMEJELFNBQVM1QyxJQUFULENBQWM2QyxTQUFkLENBQXdCQyxPQUF4QixDQUFnQyxPQUFoQyxFQUF5QyxJQUF6QyxDQUExQjs7O0FDTEE7Ozs7O0FBS0F2RCxPQUFPd0QsYUFBUCxHQUF1QixFQUF2QjtBQUNDLFdBQVV4RCxNQUFWLEVBQWtCRSxDQUFsQixFQUFxQkMsR0FBckIsRUFBMEI7O0FBRTFCO0FBQ0FBLEtBQUlDLElBQUosR0FBVyxZQUFZO0FBQ3RCRCxNQUFJRSxLQUFKOztBQUVBLE1BQUlGLElBQUlHLGlCQUFKLEVBQUosRUFBNkI7QUFDNUJILE9BQUlJLFVBQUo7QUFDQTtBQUNELEVBTkQ7O0FBUUE7QUFDQUosS0FBSUUsS0FBSixHQUFZLFlBQVk7QUFDdkJGLE1BQUlLLEVBQUosR0FBUztBQUNSQyxTQUFNUCxFQUFFLE1BQUYsQ0FERTtBQUVSRixXQUFRRSxFQUFFRixNQUFGLENBRkE7QUFHUnlELHFCQUFrQnZELEVBQUUsdURBQUYsQ0FIVjtBQUlSd0Qsd0JBQXFCeEQsRUFBRSxrQ0FBRixDQUpiO0FBS1J5RCxzQkFBbUJ6RCxFQUFFLHVGQUFGLENBTFg7QUFNUjBELHVCQUFvQjFELEVBQUUsdUJBQUY7QUFOWixHQUFUO0FBUUEsRUFURDs7QUFXQTtBQUNBQyxLQUFJSSxVQUFKLEdBQWlCLFlBQVk7QUFDNUJKLE1BQUlLLEVBQUosQ0FBT1IsTUFBUCxDQUFjVyxFQUFkLENBQWlCLE1BQWpCLEVBQXlCUixJQUFJMEQsWUFBN0I7QUFDQTFELE1BQUlLLEVBQUosQ0FBT21ELGlCQUFQLENBQXlCaEQsRUFBekIsQ0FBNEIsT0FBNUIsRUFBcUNSLElBQUkyRCxhQUF6QztBQUNBM0QsTUFBSUssRUFBSixDQUFPbUQsaUJBQVAsQ0FBeUJoRCxFQUF6QixDQUE0QixlQUE1QixFQUE2Q1IsSUFBSTRELFlBQWpEO0FBQ0E1RCxNQUFJSyxFQUFKLENBQU9vRCxrQkFBUCxDQUEwQmpELEVBQTFCLENBQTZCLGVBQTdCLEVBQThDUixJQUFJNkQsa0JBQWxEO0FBQ0EsRUFMRDs7QUFPQTtBQUNBN0QsS0FBSUcsaUJBQUosR0FBd0IsWUFBWTtBQUNuQyxTQUFPSCxJQUFJSyxFQUFKLENBQU9pRCxnQkFBUCxDQUF3QjNDLE1BQS9CO0FBQ0EsRUFGRDs7QUFJQTtBQUNBWCxLQUFJNEQsWUFBSixHQUFtQixZQUFZOztBQUU5QjtBQUNBO0FBQ0EsTUFBSTdELEVBQUUsSUFBRixFQUFRK0QsRUFBUixDQUFXLDJCQUFYLEtBQTJDLENBQUMvRCxFQUFFLElBQUYsRUFBUWlCLFFBQVIsQ0FBaUIsWUFBakIsQ0FBaEQsRUFBZ0Y7QUFDL0VqQixLQUFFLElBQUYsRUFBUXlCLElBQVIsQ0FBYSxhQUFiLEVBQTRCUCxXQUE1QixDQUF3Qyx5QkFBeEM7QUFDQTtBQUVELEVBUkQ7O0FBVUE7QUFDQWpCLEtBQUkrRCxnQkFBSixHQUF1QixVQUFVQyxFQUFWLEVBQWM7O0FBRXBDO0FBQ0EsTUFBSUEsR0FBR0MsTUFBSCxHQUFZakQsUUFBWixDQUFxQixZQUFyQixLQUFzQyxDQUFDZ0QsR0FBR2hELFFBQUgsQ0FBWSxZQUFaLENBQTNDLEVBQXNFO0FBQ3JFO0FBQ0E7O0FBRUQ7QUFDQSxNQUFJZ0QsR0FBR0MsTUFBSCxHQUFZakQsUUFBWixDQUFxQixZQUFyQixLQUFzQ2dELEdBQUdoRCxRQUFILENBQVksWUFBWixDQUExQyxFQUFxRTtBQUNwRWdELE1BQUcvQyxXQUFILENBQWUsWUFBZixFQUE2Qk8sSUFBN0IsQ0FBa0MsV0FBbEMsRUFBK0NQLFdBQS9DLENBQTJELGFBQTNELEVBQTBFVyxRQUExRSxDQUFtRixjQUFuRjtBQUNBO0FBQ0E7O0FBRUQ1QixNQUFJSyxFQUFKLENBQU9pRCxnQkFBUCxDQUF3QmpCLElBQXhCLENBQTZCLFlBQVk7O0FBRXhDO0FBQ0EsT0FBSXRDLEVBQUUsSUFBRixFQUFRaUIsUUFBUixDQUFpQixhQUFqQixDQUFKLEVBQXFDOztBQUVwQztBQUNBakIsTUFBRSxJQUFGLEVBQVFrRSxNQUFSLEdBQWlCaEQsV0FBakIsQ0FBNkIsWUFBN0IsRUFBMkNPLElBQTNDLENBQWdELG1CQUFoRCxFQUFxRUcsSUFBckUsQ0FBMEUsZUFBMUUsRUFBMkYsS0FBM0Y7O0FBRUE7QUFDQTVCLE1BQUUsSUFBRixFQUFRa0IsV0FBUixDQUFvQixhQUFwQixFQUFtQ1csUUFBbkMsQ0FBNEMsY0FBNUM7QUFDQTtBQUVELEdBWkQ7QUFhQSxFQTFCRDs7QUE0QkE7QUFDQTVCLEtBQUkwRCxZQUFKLEdBQW1CLFlBQVk7QUFDOUIxRCxNQUFJSyxFQUFKLENBQU9tRCxpQkFBUCxDQUF5QlUsT0FBekIsQ0FBaUMsMElBQWpDO0FBQ0EsRUFGRDs7QUFJQTtBQUNBbEUsS0FBSTJELGFBQUosR0FBb0IsVUFBVVEsQ0FBVixFQUFhOztBQUVoQyxNQUFJSCxLQUFLakUsRUFBRSxJQUFGLENBQVQ7QUFBQSxNQUFrQjtBQUNqQnFFLFlBQVVKLEdBQUdLLFFBQUgsQ0FBWSxhQUFaLENBRFg7QUFBQSxNQUN1QztBQUN0Q0MsWUFBVXZFLEVBQUVvRSxFQUFFckQsTUFBSixDQUZYLENBRmdDLENBSVI7O0FBRXhCO0FBQ0E7QUFDQSxNQUFJd0QsUUFBUXRELFFBQVIsQ0FBaUIsWUFBakIsS0FBa0NzRCxRQUFRdEQsUUFBUixDQUFpQixrQkFBakIsQ0FBdEMsRUFBNEU7O0FBRTNFO0FBQ0FoQixPQUFJK0QsZ0JBQUosQ0FBcUJDLEVBQXJCOztBQUVBLE9BQUksQ0FBQ0ksUUFBUXBELFFBQVIsQ0FBaUIsWUFBakIsQ0FBTCxFQUFxQzs7QUFFcEM7QUFDQWhCLFFBQUl1RSxXQUFKLENBQWdCUCxFQUFoQixFQUFvQkksT0FBcEI7QUFFQTs7QUFFRCxVQUFPLEtBQVA7QUFDQTtBQUVELEVBdkJEOztBQXlCQTtBQUNBcEUsS0FBSXVFLFdBQUosR0FBa0IsVUFBVU4sTUFBVixFQUFrQkcsT0FBbEIsRUFBMkI7O0FBRTVDO0FBQ0FILFNBQU9yQyxRQUFQLENBQWdCLFlBQWhCLEVBQThCSixJQUE5QixDQUFtQyxtQkFBbkMsRUFBd0RHLElBQXhELENBQTZELGVBQTdELEVBQThFLElBQTlFOztBQUVBO0FBQ0F5QyxVQUFReEMsUUFBUixDQUFpQixpQ0FBakI7QUFDQSxFQVBEOztBQVNBO0FBQ0E1QixLQUFJNkQsa0JBQUosR0FBeUIsWUFBWTs7QUFFcEM7QUFDQSxNQUFJLENBQUM5RCxFQUFFLElBQUYsRUFBUWlCLFFBQVIsQ0FBaUIsWUFBakIsQ0FBTCxFQUFxQztBQUNwQ2hCLE9BQUlLLEVBQUosQ0FBT21ELGlCQUFQLENBQXlCdkMsV0FBekIsQ0FBcUMsWUFBckMsRUFBbURPLElBQW5ELENBQXdELG1CQUF4RCxFQUE2RUcsSUFBN0UsQ0FBa0YsZUFBbEYsRUFBbUcsS0FBbkc7QUFDQTNCLE9BQUlLLEVBQUosQ0FBT2lELGdCQUFQLENBQXdCckMsV0FBeEIsQ0FBb0Msd0JBQXBDO0FBQ0FqQixPQUFJSyxFQUFKLENBQU9DLElBQVAsQ0FBWWtFLEdBQVosQ0FBZ0IsVUFBaEIsRUFBNEIsU0FBNUI7QUFDQXhFLE9BQUlLLEVBQUosQ0FBT0MsSUFBUCxDQUFZbUUsTUFBWixDQUFtQixZQUFuQjtBQUNBOztBQUVELE1BQUkxRSxFQUFFLElBQUYsRUFBUWlCLFFBQVIsQ0FBaUIsWUFBakIsQ0FBSixFQUFvQztBQUNuQ2hCLE9BQUlLLEVBQUosQ0FBT0MsSUFBUCxDQUFZa0UsR0FBWixDQUFnQixVQUFoQixFQUE0QixRQUE1QjtBQUNBeEUsT0FBSUssRUFBSixDQUFPQyxJQUFQLENBQVlvRSxJQUFaLENBQWlCLFlBQWpCLEVBQStCLFVBQVVQLENBQVYsRUFBYTtBQUMzQyxRQUFJLENBQUNwRSxFQUFFb0UsRUFBRXJELE1BQUosRUFBWUMsT0FBWixDQUFvQixnQkFBcEIsRUFBc0MsQ0FBdEMsQ0FBTCxFQUErQztBQUM5Q29ELE9BQUVRLGNBQUY7QUFDQTtBQUNELElBSkQ7QUFLQTtBQUNELEVBbEJEOztBQW9CQTtBQUNBNUUsR0FBRUMsSUFBSUMsSUFBTjtBQUVBLENBN0lBLEVBNklDSixNQTdJRCxFQTZJU3FCLE1BN0lULEVBNklpQnJCLE9BQU93RCxhQTdJeEIsQ0FBRDs7O0FDTkE7Ozs7O0FBS0F4RCxPQUFPK0UsUUFBUCxHQUFrQixFQUFsQjtBQUNDLFdBQVUvRSxNQUFWLEVBQWtCRSxDQUFsQixFQUFxQkMsR0FBckIsRUFBMEI7O0FBRTFCLEtBQUk2RSxxQkFBSjtBQUFBLEtBQ0NDLDJCQUREO0FBQUEsS0FFQ0MsZ0JBRkQ7QUFBQSxLQUdDQyxPQUFPOUIsU0FBUytCLGFBQVQsQ0FBdUIsUUFBdkIsQ0FIUjtBQUFBLEtBSUNDLGtCQUFrQmhDLFNBQVNpQyxvQkFBVCxDQUE4QixRQUE5QixFQUF3QyxDQUF4QyxDQUpuQjtBQUFBLEtBS0NDLFdBTEQ7O0FBT0E7QUFDQXBGLEtBQUlDLElBQUosR0FBVyxZQUFZO0FBQ3RCRCxNQUFJRSxLQUFKOztBQUVBLE1BQUlGLElBQUlHLGlCQUFKLEVBQUosRUFBNkI7QUFDNUIrRSxtQkFBZ0JHLFVBQWhCLENBQTJCQyxZQUEzQixDQUF3Q04sSUFBeEMsRUFBOENFLGVBQTlDO0FBQ0FsRixPQUFJSSxVQUFKO0FBQ0E7QUFDRCxFQVBEOztBQVNBO0FBQ0FKLEtBQUlFLEtBQUosR0FBWSxZQUFZO0FBQ3ZCRixNQUFJSyxFQUFKLEdBQVM7QUFDUixXQUFRTixFQUFFLE1BQUY7QUFEQSxHQUFUO0FBR0EsRUFKRDs7QUFNQTtBQUNBQyxLQUFJRyxpQkFBSixHQUF3QixZQUFZO0FBQ25DLFNBQU9KLEVBQUUsZ0JBQUYsRUFBb0JZLE1BQTNCO0FBQ0EsRUFGRDs7QUFJQTtBQUNBWCxLQUFJSSxVQUFKLEdBQWlCLFlBQVk7O0FBRTVCO0FBQ0FKLE1BQUlLLEVBQUosQ0FBT0MsSUFBUCxDQUFZRSxFQUFaLENBQWUsa0JBQWYsRUFBbUMsZ0JBQW5DLEVBQXFEUixJQUFJdUYsU0FBekQ7O0FBRUE7QUFDQXZGLE1BQUlLLEVBQUosQ0FBT0MsSUFBUCxDQUFZRSxFQUFaLENBQWUsa0JBQWYsRUFBbUMsUUFBbkMsRUFBNkNSLElBQUl3RixVQUFqRDs7QUFFQTtBQUNBeEYsTUFBSUssRUFBSixDQUFPQyxJQUFQLENBQVlFLEVBQVosQ0FBZSxTQUFmLEVBQTBCUixJQUFJeUYsV0FBOUI7O0FBRUE7QUFDQXpGLE1BQUlLLEVBQUosQ0FBT0MsSUFBUCxDQUFZRSxFQUFaLENBQWUsa0JBQWYsRUFBbUMsZ0JBQW5DLEVBQXFEUixJQUFJMEYsaUJBQXpEOztBQUVBO0FBQ0ExRixNQUFJSyxFQUFKLENBQU9DLElBQVAsQ0FBWUUsRUFBWixDQUFlLFNBQWYsRUFBMEJSLElBQUkyRixpQkFBOUI7QUFFQSxFQWpCRDs7QUFtQkE7QUFDQTNGLEtBQUl1RixTQUFKLEdBQWdCLFlBQVk7O0FBRTNCO0FBQ0FWLGlCQUFlOUUsRUFBRSxJQUFGLENBQWY7O0FBRUE7QUFDQSxNQUFJNkYsU0FBUzdGLEVBQUVBLEVBQUUsSUFBRixFQUFROEYsSUFBUixDQUFhLFFBQWIsQ0FBRixDQUFiOztBQUVBO0FBQ0FELFNBQU9oRSxRQUFQLENBQWdCLFlBQWhCOztBQUVBO0FBQ0E1QixNQUFJSyxFQUFKLENBQU9DLElBQVAsQ0FBWXNCLFFBQVosQ0FBcUIsWUFBckI7O0FBRUE7QUFDQTtBQUNBO0FBQ0FrRCx1QkFBcUJjLE9BQU9wRSxJQUFQLENBQVksdUJBQVosQ0FBckI7O0FBRUE7QUFDQSxNQUFJLElBQUlzRCxtQkFBbUJuRSxNQUEzQixFQUFtQzs7QUFFbEM7QUFDQW1FLHNCQUFtQixDQUFuQixFQUFzQmdCLEtBQXRCO0FBQ0E7QUFFRCxFQTFCRDs7QUE0QkE7QUFDQTlGLEtBQUl3RixVQUFKLEdBQWlCLFlBQVk7O0FBRTVCO0FBQ0EsTUFBSUksU0FBUzdGLEVBQUVBLEVBQUUsdUJBQUYsRUFBMkI4RixJQUEzQixDQUFnQyxRQUFoQyxDQUFGLENBQWI7OztBQUVDO0FBQ0FFLFlBQVVILE9BQU9wRSxJQUFQLENBQVksUUFBWixDQUhYOztBQUtBO0FBQ0EsTUFBSXVFLFFBQVFwRixNQUFaLEVBQW9COztBQUVuQjtBQUNBLE9BQUlxRixNQUFNRCxRQUFRcEUsSUFBUixDQUFhLEtBQWIsQ0FBVjs7QUFFQTtBQUNBO0FBQ0EsT0FBSSxDQUFDcUUsSUFBSUMsUUFBSixDQUFhLGVBQWIsQ0FBTCxFQUFvQzs7QUFFbkM7QUFDQUYsWUFBUXBFLElBQVIsQ0FBYSxLQUFiLEVBQW9CLEVBQXBCLEVBQXdCQSxJQUF4QixDQUE2QixLQUE3QixFQUFvQ3FFLEdBQXBDO0FBQ0EsSUFKRCxNQUlPOztBQUVOO0FBQ0FqQixZQUFRbUIsU0FBUjtBQUNBO0FBQ0Q7O0FBRUQ7QUFDQU4sU0FBTzNFLFdBQVAsQ0FBbUIsWUFBbkI7O0FBRUE7QUFDQWpCLE1BQUlLLEVBQUosQ0FBT0MsSUFBUCxDQUFZVyxXQUFaLENBQXdCLFlBQXhCOztBQUVBO0FBQ0E0RCxlQUFhaUIsS0FBYjtBQUVBLEVBcENEOztBQXNDQTtBQUNBOUYsS0FBSXlGLFdBQUosR0FBa0IsVUFBVTVFLEtBQVYsRUFBaUI7QUFDbEMsTUFBSSxPQUFPQSxNQUFNc0YsT0FBakIsRUFBMEI7QUFDekJuRyxPQUFJd0YsVUFBSjtBQUNBO0FBQ0QsRUFKRDs7QUFNQTtBQUNBeEYsS0FBSTBGLGlCQUFKLEdBQXdCLFVBQVU3RSxLQUFWLEVBQWlCOztBQUV4QztBQUNBLE1BQUksQ0FBQ2QsRUFBRWMsTUFBTUMsTUFBUixFQUFnQkMsT0FBaEIsQ0FBd0IsS0FBeEIsRUFBK0JDLFFBQS9CLENBQXdDLGNBQXhDLENBQUwsRUFBOEQ7QUFDN0RoQixPQUFJd0YsVUFBSjtBQUNBO0FBQ0QsRUFORDs7QUFRQTtBQUNBeEYsS0FBSTJGLGlCQUFKLEdBQXdCLFVBQVU5RSxLQUFWLEVBQWlCOztBQUV4QztBQUNBLE1BQUksTUFBTUEsTUFBTXVGLEtBQVosSUFBcUIsSUFBSXJHLEVBQUUsYUFBRixFQUFpQlksTUFBOUMsRUFBc0Q7QUFDckQsT0FBSTBGLFdBQVd0RyxFQUFFLFFBQUYsQ0FBZjtBQUFBLE9BQ0N1RyxhQUFheEIsbUJBQW1CeUIsS0FBbkIsQ0FBeUJGLFFBQXpCLENBRGQ7O0FBR0EsT0FBSSxNQUFNQyxVQUFOLElBQW9CekYsTUFBTTJGLFFBQTlCLEVBQXdDOztBQUV2QztBQUNBMUIsdUJBQW1CQSxtQkFBbUJuRSxNQUFuQixHQUE0QixDQUEvQyxFQUFrRG1GLEtBQWxEO0FBQ0FqRixVQUFNOEQsY0FBTjtBQUNBLElBTEQsTUFLTyxJQUFJLENBQUM5RCxNQUFNMkYsUUFBUCxJQUFtQkYsZUFBZXhCLG1CQUFtQm5FLE1BQW5CLEdBQTRCLENBQWxFLEVBQXFFOztBQUUzRTtBQUNBbUUsdUJBQW1CLENBQW5CLEVBQXNCZ0IsS0FBdEI7QUFDQWpGLFVBQU04RCxjQUFOO0FBQ0E7QUFDRDtBQUNELEVBbkJEOztBQXFCQTtBQUNBM0UsS0FBSXlHLHVCQUFKLEdBQThCLFlBQVk7QUFDekMsTUFBSWIsU0FBUzdGLEVBQUUsV0FBRixDQUFiO0FBQUEsTUFDQzJHLFlBQVlkLE9BQU9wRSxJQUFQLENBQVksUUFBWixFQUFzQkcsSUFBdEIsQ0FBMkIsSUFBM0IsQ0FEYjs7QUFHQW9ELFlBQVUsSUFBSUssR0FBR3VCLE1BQVAsQ0FBY0QsU0FBZCxFQUF5QjtBQUNsQ0UsV0FBUTtBQUNQLGVBQVc1RyxJQUFJNkcsYUFEUjtBQUVQLHFCQUFpQjdHLElBQUk4RztBQUZkO0FBRDBCLEdBQXpCLENBQVY7QUFNQSxFQVZEOztBQVlBO0FBQ0E5RyxLQUFJNkcsYUFBSixHQUFvQixZQUFZLENBQUUsQ0FBbEM7O0FBRUE7QUFDQTdHLEtBQUk4RyxtQkFBSixHQUEwQixZQUFZOztBQUVyQztBQUNBL0csSUFBRWMsTUFBTUMsTUFBTixDQUFhaUcsQ0FBZixFQUFrQmhHLE9BQWxCLENBQTBCLFFBQTFCLEVBQW9DUyxJQUFwQyxDQUF5Qyx1QkFBekMsRUFBa0V3RixLQUFsRSxHQUEwRWxCLEtBQTFFO0FBQ0EsRUFKRDs7QUFPQTtBQUNBL0YsR0FBRUMsSUFBSUMsSUFBTjtBQUNBLENBdkxBLEVBdUxDSixNQXZMRCxFQXVMU3FCLE1BdkxULEVBdUxpQnJCLE9BQU8rRSxRQXZMeEIsQ0FBRDs7O0FDTkE7Ozs7O0FBS0EvRSxPQUFPb0gsb0JBQVAsR0FBOEIsRUFBOUI7QUFDQyxXQUFVcEgsTUFBVixFQUFrQkUsQ0FBbEIsRUFBcUJDLEdBQXJCLEVBQTBCOztBQUUxQjtBQUNBQSxLQUFJQyxJQUFKLEdBQVcsWUFBWTtBQUN0QkQsTUFBSUUsS0FBSjs7QUFFQSxNQUFJRixJQUFJRyxpQkFBSixFQUFKLEVBQTZCO0FBQzVCSCxPQUFJSSxVQUFKO0FBQ0E7QUFDRCxFQU5EOztBQVFBO0FBQ0FKLEtBQUlFLEtBQUosR0FBWSxZQUFZO0FBQ3ZCRixNQUFJSyxFQUFKLEdBQVM7QUFDUlIsV0FBUUUsRUFBRUYsTUFBRixDQURBO0FBRVJ5RCxxQkFBa0J2RCxFQUFFLDRCQUFGLENBRlY7QUFHUnlELHNCQUFtQnpELEVBQUUsNENBQUY7QUFIWCxHQUFUO0FBS0EsRUFORDs7QUFRQTtBQUNBQyxLQUFJSSxVQUFKLEdBQWlCLFlBQVk7QUFDNUJKLE1BQUlLLEVBQUosQ0FBT1IsTUFBUCxDQUFjVyxFQUFkLENBQWlCLE1BQWpCLEVBQXlCUixJQUFJMEQsWUFBN0I7QUFDQTFELE1BQUlLLEVBQUosQ0FBT21ELGlCQUFQLENBQXlCaEMsSUFBekIsQ0FBOEIsR0FBOUIsRUFBbUNoQixFQUFuQyxDQUFzQyxrQkFBdEMsRUFBMERSLElBQUlrSCxXQUE5RDtBQUNBLEVBSEQ7O0FBS0E7QUFDQWxILEtBQUlHLGlCQUFKLEdBQXdCLFlBQVk7QUFDbkMsU0FBT0gsSUFBSUssRUFBSixDQUFPaUQsZ0JBQVAsQ0FBd0IzQyxNQUEvQjtBQUNBLEVBRkQ7O0FBSUE7QUFDQVgsS0FBSTBELFlBQUosR0FBbUIsWUFBWTtBQUM5QjFELE1BQUlLLEVBQUosQ0FBT21ELGlCQUFQLENBQXlCaEMsSUFBekIsQ0FBOEIsS0FBOUIsRUFBcUMyRixNQUFyQyxDQUE0QyxxREFBNUM7QUFDQSxFQUZEOztBQUlBO0FBQ0FuSCxLQUFJa0gsV0FBSixHQUFrQixZQUFZO0FBQzdCbkgsSUFBRSxJQUFGLEVBQVFnQixPQUFSLENBQWdCLDJCQUFoQixFQUE2Q0gsV0FBN0MsQ0FBeUQsT0FBekQ7QUFDQSxFQUZEOztBQUlBO0FBQ0FiLEdBQUVDLElBQUlDLElBQU47QUFFQSxDQTVDQSxFQTRDQ0osTUE1Q0QsRUE0Q1NxQixNQTVDVCxFQTRDaUJyQixPQUFPb0gsb0JBNUN4QixDQUFEOzs7QUNOQTs7Ozs7QUFLQXBILE9BQU91SCxZQUFQLEdBQXNCLEVBQXRCO0FBQ0MsV0FBVXZILE1BQVYsRUFBa0JFLENBQWxCLEVBQXFCQyxHQUFyQixFQUEwQjs7QUFFMUI7QUFDQUEsS0FBSUMsSUFBSixHQUFXLFlBQVk7QUFDdEJELE1BQUlFLEtBQUo7O0FBRUEsTUFBSUYsSUFBSUcsaUJBQUosRUFBSixFQUE2QjtBQUM1QkgsT0FBSUksVUFBSjtBQUNBO0FBQ0QsRUFORDs7QUFRQTtBQUNBSixLQUFJRSxLQUFKLEdBQVksWUFBWTtBQUN2QkYsTUFBSUssRUFBSixHQUFTO0FBQ1JDLFNBQU1QLEVBQUUsTUFBRixDQURFO0FBRVJzSCxtQkFBZ0J0SCxFQUFFLG1CQUFGLENBRlI7QUFHUjBELHVCQUFvQjFELEVBQUUsdUJBQUYsQ0FIWjtBQUlSdUgsa0JBQWV2SCxFQUFFLGtCQUFGLENBSlA7QUFLUndILG9CQUFpQnhILEVBQUUsb0JBQUY7QUFMVCxHQUFUO0FBT0EsRUFSRDs7QUFVQTtBQUNBQyxLQUFJSSxVQUFKLEdBQWlCLFlBQVk7QUFDNUJKLE1BQUlLLEVBQUosQ0FBT0MsSUFBUCxDQUFZRSxFQUFaLENBQWUsU0FBZixFQUEwQlIsSUFBSXlGLFdBQTlCO0FBQ0F6RixNQUFJSyxFQUFKLENBQU9nSCxjQUFQLENBQXNCN0csRUFBdEIsQ0FBeUIsT0FBekIsRUFBa0NSLElBQUl3SCxjQUF0QztBQUNBeEgsTUFBSUssRUFBSixDQUFPaUgsYUFBUCxDQUFxQjlHLEVBQXJCLENBQXdCLE9BQXhCLEVBQWlDUixJQUFJeUgsZUFBckM7QUFDQXpILE1BQUlLLEVBQUosQ0FBT2tILGVBQVAsQ0FBdUIvRyxFQUF2QixDQUEwQixPQUExQixFQUFtQ1IsSUFBSXdILGNBQXZDO0FBQ0EsRUFMRDs7QUFPQTtBQUNBeEgsS0FBSUcsaUJBQUosR0FBd0IsWUFBWTtBQUNuQyxTQUFPSCxJQUFJSyxFQUFKLENBQU9vRCxrQkFBUCxDQUEwQjlDLE1BQWpDO0FBQ0EsRUFGRDs7QUFJQTtBQUNBWCxLQUFJeUgsZUFBSixHQUFzQixZQUFZOztBQUVqQyxNQUFJLFdBQVcxSCxFQUFFLElBQUYsRUFBUTRCLElBQVIsQ0FBYSxlQUFiLENBQWYsRUFBOEM7QUFDN0MzQixPQUFJd0gsY0FBSjtBQUNBLEdBRkQsTUFFTztBQUNOeEgsT0FBSTBILGFBQUo7QUFDQTtBQUVELEVBUkQ7O0FBVUE7QUFDQTFILEtBQUkwSCxhQUFKLEdBQW9CLFlBQVk7QUFDL0IxSCxNQUFJSyxFQUFKLENBQU9vRCxrQkFBUCxDQUEwQjdCLFFBQTFCLENBQW1DLFlBQW5DO0FBQ0E1QixNQUFJSyxFQUFKLENBQU9pSCxhQUFQLENBQXFCMUYsUUFBckIsQ0FBOEIsWUFBOUI7QUFDQTVCLE1BQUlLLEVBQUosQ0FBT2tILGVBQVAsQ0FBdUIzRixRQUF2QixDQUFnQyxZQUFoQzs7QUFFQTVCLE1BQUlLLEVBQUosQ0FBT2lILGFBQVAsQ0FBcUIzRixJQUFyQixDQUEwQixlQUExQixFQUEyQyxJQUEzQztBQUNBM0IsTUFBSUssRUFBSixDQUFPb0Qsa0JBQVAsQ0FBMEI5QixJQUExQixDQUErQixhQUEvQixFQUE4QyxLQUE5Qzs7QUFFQTNCLE1BQUlLLEVBQUosQ0FBT29ELGtCQUFQLENBQTBCakMsSUFBMUIsQ0FBK0IsUUFBL0IsRUFBeUN3RixLQUF6QyxHQUFpRGxCLEtBQWpEO0FBQ0EsRUFURDs7QUFXQTtBQUNBOUYsS0FBSXdILGNBQUosR0FBcUIsWUFBWTtBQUNoQ3hILE1BQUlLLEVBQUosQ0FBT29ELGtCQUFQLENBQTBCeEMsV0FBMUIsQ0FBc0MsWUFBdEM7QUFDQWpCLE1BQUlLLEVBQUosQ0FBT2lILGFBQVAsQ0FBcUJyRyxXQUFyQixDQUFpQyxZQUFqQztBQUNBakIsTUFBSUssRUFBSixDQUFPa0gsZUFBUCxDQUF1QnRHLFdBQXZCLENBQW1DLFlBQW5DOztBQUVBakIsTUFBSUssRUFBSixDQUFPaUgsYUFBUCxDQUFxQjNGLElBQXJCLENBQTBCLGVBQTFCLEVBQTJDLEtBQTNDO0FBQ0EzQixNQUFJSyxFQUFKLENBQU9vRCxrQkFBUCxDQUEwQjlCLElBQTFCLENBQStCLGFBQS9CLEVBQThDLElBQTlDOztBQUVBM0IsTUFBSUssRUFBSixDQUFPaUgsYUFBUCxDQUFxQnhCLEtBQXJCO0FBQ0EsRUFURDs7QUFXQTtBQUNBOUYsS0FBSXlGLFdBQUosR0FBa0IsVUFBVTVFLEtBQVYsRUFBaUI7QUFDbEMsTUFBSSxPQUFPQSxNQUFNc0YsT0FBakIsRUFBMEI7QUFDekJuRyxPQUFJd0gsY0FBSjtBQUNBO0FBQ0QsRUFKRDs7QUFNQTtBQUNBekgsR0FBRUMsSUFBSUMsSUFBTjtBQUVBLENBaEZBLEVBZ0ZDSixNQWhGRCxFQWdGU3FCLE1BaEZULEVBZ0ZpQnJCLE9BQU91SCxZQWhGeEIsQ0FBRDs7O0FDTkE7Ozs7Ozs7QUFPQyxhQUFZO0FBQ1osS0FBSU8sV0FBVyxDQUFDLENBQUQsR0FBS0MsVUFBVUMsU0FBVixDQUFvQkMsV0FBcEIsR0FBa0NDLE9BQWxDLENBQTBDLFFBQTFDLENBQXBCO0FBQUEsS0FDQ0MsVUFBVSxDQUFDLENBQUQsR0FBS0osVUFBVUMsU0FBVixDQUFvQkMsV0FBcEIsR0FBa0NDLE9BQWxDLENBQTBDLE9BQTFDLENBRGhCO0FBQUEsS0FFQ0UsT0FBTyxDQUFDLENBQUQsR0FBS0wsVUFBVUMsU0FBVixDQUFvQkMsV0FBcEIsR0FBa0NDLE9BQWxDLENBQTBDLE1BQTFDLENBRmI7O0FBSUEsS0FBSSxDQUFDSixZQUFZSyxPQUFaLElBQXVCQyxJQUF4QixLQUFpQy9FLFNBQVNnRixjQUExQyxJQUE0RHJJLE9BQU9zSSxnQkFBdkUsRUFBeUY7QUFDeEZ0SSxTQUFPc0ksZ0JBQVAsQ0FBd0IsWUFBeEIsRUFBc0MsWUFBWTtBQUNqRCxPQUFJQyxLQUFLQyxTQUFTQyxJQUFULENBQWNDLFNBQWQsQ0FBd0IsQ0FBeEIsQ0FBVDtBQUFBLE9BQ0NDLE9BREQ7O0FBR0EsT0FBSSxDQUFFLGVBQUQsQ0FBa0JDLElBQWxCLENBQXVCTCxFQUF2QixDQUFMLEVBQWlDO0FBQ2hDO0FBQ0E7O0FBRURJLGFBQVV0RixTQUFTZ0YsY0FBVCxDQUF3QkUsRUFBeEIsQ0FBVjs7QUFFQSxPQUFJSSxPQUFKLEVBQWE7QUFDWixRQUFJLENBQUUsdUNBQUQsQ0FBMENDLElBQTFDLENBQStDRCxRQUFRRSxPQUF2RCxDQUFMLEVBQXNFO0FBQ3JFRixhQUFRRyxRQUFSLEdBQW1CLENBQUMsQ0FBcEI7QUFDQTs7QUFFREgsWUFBUTFDLEtBQVI7QUFDQTtBQUNELEdBakJELEVBaUJHLEtBakJIO0FBa0JBO0FBQ0QsQ0F6QkEsR0FBRDs7O0FDUEE7Ozs7O0FBS0FqRyxPQUFPK0ksY0FBUCxHQUF3QixFQUF4QjtBQUNDLFdBQVUvSSxNQUFWLEVBQWtCRSxDQUFsQixFQUFxQkMsR0FBckIsRUFBMEI7O0FBRTFCO0FBQ0FBLEtBQUlDLElBQUosR0FBVyxZQUFZO0FBQ3RCRCxNQUFJRSxLQUFKO0FBQ0FGLE1BQUlJLFVBQUo7QUFDQSxFQUhEOztBQUtBO0FBQ0FKLEtBQUlFLEtBQUosR0FBWSxZQUFZO0FBQ3ZCRixNQUFJSyxFQUFKLEdBQVM7QUFDUixhQUFVTixFQUFFRixNQUFGLENBREY7QUFFUixXQUFRRSxFQUFFbUQsU0FBUzVDLElBQVg7QUFGQSxHQUFUO0FBSUEsRUFMRDs7QUFPQTtBQUNBTixLQUFJSSxVQUFKLEdBQWlCLFlBQVk7QUFDNUJKLE1BQUlLLEVBQUosQ0FBT1IsTUFBUCxDQUFjZ0osSUFBZCxDQUFtQjdJLElBQUk4SSxZQUF2QjtBQUNBLEVBRkQ7O0FBSUE7QUFDQTlJLEtBQUk4SSxZQUFKLEdBQW1CLFlBQVk7QUFDOUI5SSxNQUFJSyxFQUFKLENBQU9DLElBQVAsQ0FBWXNCLFFBQVosQ0FBcUIsT0FBckI7QUFDQSxFQUZEOztBQUlBO0FBQ0E3QixHQUFFQyxJQUFJQyxJQUFOO0FBQ0EsQ0E1QkEsRUE0QkNKLE1BNUJELEVBNEJTcUIsTUE1QlQsRUE0QmlCckIsT0FBTytJLGNBNUJ4QixDQUFEIiwiZmlsZSI6InByb2plY3QuanMiLCJzb3VyY2VzQ29udGVudCI6WyIvKipcbiAqIFNob3cvSGlkZSB0aGUgU2VhcmNoIEZvcm0gaW4gdGhlIGhlYWRlci5cbiAqXG4gKiBAYXV0aG9yIENvcmV5IENvbGxpbnNcbiAqL1xud2luZG93LlNob3dIaWRlU2VhcmNoRm9ybSA9IHt9O1xuKGZ1bmN0aW9uICh3aW5kb3csICQsIGFwcCkge1xuXG5cdC8vIENvbnN0cnVjdG9yXG5cdGFwcC5pbml0ID0gZnVuY3Rpb24gKCkge1xuXHRcdGFwcC5jYWNoZSgpO1xuXG5cdFx0aWYgKGFwcC5tZWV0c1JlcXVpcmVtZW50cygpKSB7XG5cdFx0XHRhcHAuYmluZEV2ZW50cygpO1xuXHRcdH1cblx0fTtcblxuXHQvLyBDYWNoZSBhbGwgdGhlIHRoaW5nc1xuXHRhcHAuY2FjaGUgPSBmdW5jdGlvbiAoKSB7XG5cdFx0YXBwLiRjID0ge1xuXHRcdFx0d2luZG93OiAkKHdpbmRvdyksXG5cdFx0XHRib2R5OiAkKCdib2R5JyksXG5cdFx0XHRoZWFkZXJTZWFyY2hGb3JtOiAkKCcuc2l0ZS1oZWFkZXItYWN0aW9uIC5jdGEtYnV0dG9uJylcblx0XHR9O1xuXHR9O1xuXG5cdC8vIENvbWJpbmUgYWxsIGV2ZW50c1xuXHRhcHAuYmluZEV2ZW50cyA9IGZ1bmN0aW9uICgpIHtcblx0XHRhcHAuJGMuaGVhZGVyU2VhcmNoRm9ybS5vbigna2V5dXAgdG91Y2hzdGFydCBjbGljaycsIGFwcC5zaG93SGlkZVNlYXJjaEZvcm0pO1xuXHRcdGFwcC4kYy5ib2R5Lm9uKCdrZXl1cCB0b3VjaHN0YXJ0IGNsaWNrJywgYXBwLmhpZGVTZWFyY2hGb3JtKTtcblx0fTtcblxuXHQvLyBEbyB3ZSBtZWV0IHRoZSByZXF1aXJlbWVudHM/XG5cdGFwcC5tZWV0c1JlcXVpcmVtZW50cyA9IGZ1bmN0aW9uICgpIHtcblx0XHRyZXR1cm4gYXBwLiRjLmhlYWRlclNlYXJjaEZvcm0ubGVuZ3RoO1xuXHR9O1xuXG5cdC8vIEFkZHMgdGhlIHRvZ2dsZSBjbGFzcyBmb3IgdGhlIHNlYXJjaCBmb3JtLlxuXHRhcHAuc2hvd0hpZGVTZWFyY2hGb3JtID0gZnVuY3Rpb24gKCkge1xuXHRcdGFwcC4kYy5ib2R5LnRvZ2dsZUNsYXNzKCdzZWFyY2gtZm9ybS12aXNpYmxlJyk7XG5cdH07XG5cblx0Ly8gSGlkZXMgdGhlIHNlYXJjaCBmb3JtIGlmIHdlIGNsaWNrIG91dHNpZGUgb2YgaXRzIGNvbnRhaW5lci5cblx0YXBwLmhpZGVTZWFyY2hGb3JtID0gZnVuY3Rpb24gKGV2ZW50KSB7XG5cblx0XHRpZiAoISQoZXZlbnQudGFyZ2V0KS5wYXJlbnRzKCdkaXYnKS5oYXNDbGFzcygnc2l0ZS1oZWFkZXItYWN0aW9uJykpIHtcblx0XHRcdGFwcC4kYy5ib2R5LnJlbW92ZUNsYXNzKCdzZWFyY2gtZm9ybS12aXNpYmxlJyk7XG5cdFx0fVxuXHR9O1xuXG5cdC8vIEVuZ2FnZVxuXHQkKGFwcC5pbml0KTtcblxufSh3aW5kb3csIGpRdWVyeSwgd2luZG93LlNob3dIaWRlU2VhcmNoRm9ybSkpOyIsIi8qKlxuICogRmlsZSBoZXJvLWNhcm91c2VsLmpzXG4gKlxuICogQ3JlYXRlIGEgY2Fyb3VzZWwgaWYgd2UgaGF2ZSBtb3JlIHRoYW4gb25lIGhlcm8gc2xpZGUuXG4gKi9cbndpbmRvdy53ZHNIZXJvQ2Fyb3VzZWwgPSB7fTtcbihmdW5jdGlvbiAod2luZG93LCAkLCBhcHApIHtcblxuXHQvLyBDb25zdHJ1Y3Rvci5cblx0YXBwLmluaXQgPSBmdW5jdGlvbiAoKSB7XG5cdFx0YXBwLmNhY2hlKCk7XG5cblx0XHRpZiAoYXBwLm1lZXRzUmVxdWlyZW1lbnRzKCkpIHtcblx0XHRcdGFwcC5iaW5kRXZlbnRzKCk7XG5cdFx0fVxuXHR9O1xuXG5cdC8vIENhY2hlIGFsbCB0aGUgdGhpbmdzLlxuXHRhcHAuY2FjaGUgPSBmdW5jdGlvbiAoKSB7XG5cdFx0YXBwLiRjID0ge1xuXHRcdFx0d2luZG93OiAkKHdpbmRvdyksXG5cdFx0XHRoZXJvQ2Fyb3VzZWw6ICQoJy5jYXJvdXNlbCcpXG5cdFx0fTtcblx0fTtcblxuXHQvLyBDb21iaW5lIGFsbCBldmVudHMuXG5cdGFwcC5iaW5kRXZlbnRzID0gZnVuY3Rpb24gKCkge1xuXHRcdGFwcC4kYy53aW5kb3cub24oJ2xvYWQnLCBhcHAuZG9TbGljayk7XG5cdFx0YXBwLiRjLndpbmRvdy5vbignbG9hZCcsIGFwcC5kb0ZpcnN0QW5pbWF0aW9uKTtcblx0fTtcblxuXHQvLyBEbyB3ZSBtZWV0IHRoZSByZXF1aXJlbWVudHM/XG5cdGFwcC5tZWV0c1JlcXVpcmVtZW50cyA9IGZ1bmN0aW9uICgpIHtcblx0XHRyZXR1cm4gYXBwLiRjLmhlcm9DYXJvdXNlbC5sZW5ndGg7XG5cdH07XG5cblx0Ly8gQW5pbWF0ZSB0aGUgZmlyc3Qgc2xpZGUgb24gd2luZG93IGxvYWQuXG5cdGFwcC5kb0ZpcnN0QW5pbWF0aW9uID0gZnVuY3Rpb24gKCkge1xuXG5cdFx0Ly8gR2V0IHRoZSBmaXJzdCBzbGlkZSBjb250ZW50IGFyZWEgYW5kIGFuaW1hdGlvbiBhdHRyaWJ1dGUuXG5cdFx0bGV0IGZpcnN0U2xpZGUgPSBhcHAuJGMuaGVyb0Nhcm91c2VsLmZpbmQoJ1tkYXRhLXNsaWNrLWluZGV4PTBdJyksXG5cdFx0XHRmaXJzdFNsaWRlQ29udGVudCA9IGZpcnN0U2xpZGUuZmluZCgnLmhlcm8tY29udGVudCcpLFxuXHRcdFx0Zmlyc3RBbmltYXRpb24gPSBmaXJzdFNsaWRlQ29udGVudC5hdHRyKCdkYXRhLWFuaW1hdGlvbicpO1xuXG5cdFx0Ly8gQWRkIHRoZSBhbmltYXRpb24gY2xhc3MgdG8gdGhlIGZpcnN0IHNsaWRlLlxuXHRcdGZpcnN0U2xpZGVDb250ZW50LmFkZENsYXNzKGZpcnN0QW5pbWF0aW9uKTtcblx0fTtcblxuXHQvLyBBbmltYXRlIHRoZSBzbGlkZSBjb250ZW50LlxuXHRhcHAuZG9BbmltYXRpb24gPSBmdW5jdGlvbiAoKSB7XG5cdFx0bGV0IHNsaWRlcyA9ICQoJy5zbGlkZScpLFxuXHRcdFx0YWN0aXZlU2xpZGUgPSAkKCcuc2xpY2stY3VycmVudCcpLFxuXHRcdFx0YWN0aXZlQ29udGVudCA9IGFjdGl2ZVNsaWRlLmZpbmQoJy5oZXJvLWNvbnRlbnQnKSxcblxuXHRcdFx0Ly8gVGhpcyBpcyBhIHN0cmluZyBsaWtlIHNvOiAnYW5pbWF0ZWQgc29tZUNzc0NsYXNzJy5cblx0XHRcdGFuaW1hdGlvbkNsYXNzID0gYWN0aXZlQ29udGVudC5hdHRyKCdkYXRhLWFuaW1hdGlvbicpLFxuXHRcdFx0c3BsaXRBbmltYXRpb24gPSBhbmltYXRpb25DbGFzcy5zcGxpdCgnICcpLFxuXG5cdFx0XHQvLyBUaGlzIGlzIHRoZSAnYW5pbWF0ZWQnIGNsYXNzLlxuXHRcdFx0YW5pbWF0aW9uVHJpZ2dlciA9IHNwbGl0QW5pbWF0aW9uWzBdO1xuXG5cdFx0Ly8gR28gdGhyb3VnaCBlYWNoIHNsaWRlIHRvIHNlZSBpZiB3ZSd2ZSBhbHJlYWR5IHNldCBhbmltYXRpb24gY2xhc3Nlcy5cblx0XHRzbGlkZXMuZWFjaChmdW5jdGlvbiAoKSB7XG5cdFx0XHRsZXQgc2xpZGVDb250ZW50ID0gJCh0aGlzKS5maW5kKCcuaGVyby1jb250ZW50Jyk7XG5cblx0XHRcdC8vIElmIHdlJ3ZlIHNldCBhbmltYXRpb24gY2xhc3NlcyBvbiBhIHNsaWRlLCByZW1vdmUgdGhlbS5cblx0XHRcdGlmIChzbGlkZUNvbnRlbnQuaGFzQ2xhc3MoJ2FuaW1hdGVkJykpIHtcblxuXHRcdFx0XHQvLyBHZXQgdGhlIGxhc3QgY2xhc3MsIHdoaWNoIGlzIHRoZSBhbmltYXRlLmNzcyBjbGFzcy5cblx0XHRcdFx0bGV0IGxhc3RDbGFzcyA9IHNsaWRlQ29udGVudFxuXHRcdFx0XHRcdC5hdHRyKCdjbGFzcycpXG5cdFx0XHRcdFx0LnNwbGl0KCcgJylcblx0XHRcdFx0XHQucG9wKCk7XG5cblx0XHRcdFx0Ly8gUmVtb3ZlIGJvdGggYW5pbWF0aW9uIGNsYXNzZXMuXG5cdFx0XHRcdHNsaWRlQ29udGVudC5yZW1vdmVDbGFzcyhsYXN0Q2xhc3MpLnJlbW92ZUNsYXNzKGFuaW1hdGlvblRyaWdnZXIpO1xuXHRcdFx0fVxuXHRcdH0pO1xuXG5cdFx0Ly8gQWRkIGFuaW1hdGlvbiBjbGFzc2VzIGFmdGVyIHNsaWRlIGlzIGluIHZpZXcuXG5cdFx0YWN0aXZlQ29udGVudC5hZGRDbGFzcyhhbmltYXRpb25DbGFzcyk7XG5cdH07XG5cblx0Ly8gQWxsb3cgYmFja2dyb3VuZCB2aWRlb3MgdG8gYXV0b3BsYXkuXG5cdGFwcC5wbGF5QmFja2dyb3VuZFZpZGVvcyA9IGZ1bmN0aW9uICgpIHtcblxuXHRcdC8vIEdldCBhbGwgdGhlIHZpZGVvcyBpbiBvdXIgc2xpZGVzIG9iamVjdC5cblx0XHQkKCd2aWRlbycpLmVhY2goZnVuY3Rpb24gKCkge1xuXG5cdFx0XHQvLyBMZXQgdGhlbSBhdXRvcGxheS4gVE9ETzogUG9zc2libHkgY2hhbmdlIHRoaXMgbGF0ZXIgdG8gb25seSBwbGF5IHRoZSB2aXNpYmxlIHNsaWRlIHZpZGVvLlxuXHRcdFx0dGhpcy5wbGF5KCk7XG5cdFx0fSk7XG5cdH07XG5cblx0Ly8gS2ljayBvZmYgU2xpY2suXG5cdGFwcC5kb1NsaWNrID0gZnVuY3Rpb24gKCkge1xuXHRcdGFwcC4kYy5oZXJvQ2Fyb3VzZWwub24oJ2luaXQnLCBhcHAucGxheUJhY2tncm91bmRWaWRlb3MpO1xuXG5cdFx0YXBwLiRjLmhlcm9DYXJvdXNlbC5zbGljayh7XG5cdFx0XHRhdXRvcGxheTogdHJ1ZSxcblx0XHRcdGF1dG9wbGF5U3BlZWQ6IDUwMDAsXG5cdFx0XHRhcnJvd3M6IGZhbHNlLFxuXHRcdFx0ZG90czogZmFsc2UsXG5cdFx0XHRmb2N1c09uU2VsZWN0OiB0cnVlLFxuXHRcdFx0d2FpdEZvckFuaW1hdGU6IHRydWVcblx0XHR9KTtcblxuXHRcdGFwcC4kYy5oZXJvQ2Fyb3VzZWwub24oJ2FmdGVyQ2hhbmdlJywgYXBwLmRvQW5pbWF0aW9uKTtcblx0fTtcblxuXHQvLyBFbmdhZ2UhXG5cdCQoYXBwLmluaXQpO1xufSh3aW5kb3csIGpRdWVyeSwgd2luZG93Lndkc0hlcm9DYXJvdXNlbCkpOyIsIi8qKlxuICogRmlsZSBqcy1lbmFibGVkLmpzXG4gKlxuICogSWYgSmF2YXNjcmlwdCBpcyBlbmFibGVkLCByZXBsYWNlIHRoZSA8Ym9keT4gY2xhc3MgXCJuby1qc1wiLlxuICovXG5kb2N1bWVudC5ib2R5LmNsYXNzTmFtZSA9IGRvY3VtZW50LmJvZHkuY2xhc3NOYW1lLnJlcGxhY2UoJ25vLWpzJywgJ2pzJyk7IiwiLyoqXG4gKiBGaWxlOiBtb2JpbGUtbWVudS5qc1xuICpcbiAqIENyZWF0ZSBhbiBhY2NvcmRpb24gc3R5bGUgZHJvcGRvd24uXG4gKi9cbndpbmRvdy53ZHNNb2JpbGVNZW51ID0ge307XG4oZnVuY3Rpb24gKHdpbmRvdywgJCwgYXBwKSB7XG5cblx0Ly8gQ29uc3RydWN0b3IuXG5cdGFwcC5pbml0ID0gZnVuY3Rpb24gKCkge1xuXHRcdGFwcC5jYWNoZSgpO1xuXG5cdFx0aWYgKGFwcC5tZWV0c1JlcXVpcmVtZW50cygpKSB7XG5cdFx0XHRhcHAuYmluZEV2ZW50cygpO1xuXHRcdH1cblx0fTtcblxuXHQvLyBDYWNoZSBhbGwgdGhlIHRoaW5ncy5cblx0YXBwLmNhY2hlID0gZnVuY3Rpb24gKCkge1xuXHRcdGFwcC4kYyA9IHtcblx0XHRcdGJvZHk6ICQoJ2JvZHknKSxcblx0XHRcdHdpbmRvdzogJCh3aW5kb3cpLFxuXHRcdFx0c3ViTWVudUNvbnRhaW5lcjogJCgnLm1vYmlsZS1tZW51IC5zdWItbWVudSwgLnV0aWxpdHktbmF2aWdhdGlvbiAuc3ViLW1lbnUnKSxcblx0XHRcdHN1YlN1Yk1lbnVDb250YWluZXI6ICQoJy5tb2JpbGUtbWVudSAuc3ViLW1lbnUgLnN1Yi1tZW51JyksXG5cdFx0XHRzdWJNZW51UGFyZW50SXRlbTogJCgnLm1vYmlsZS1tZW51IGxpLm1lbnUtaXRlbS1oYXMtY2hpbGRyZW4sIC51dGlsaXR5LW5hdmlnYXRpb24gbGkubWVudS1pdGVtLWhhcy1jaGlsZHJlbicpLFxuXHRcdFx0b2ZmQ2FudmFzQ29udGFpbmVyOiAkKCcub2ZmLWNhbnZhcy1jb250YWluZXInKVxuXHRcdH07XG5cdH07XG5cblx0Ly8gQ29tYmluZSBhbGwgZXZlbnRzLlxuXHRhcHAuYmluZEV2ZW50cyA9IGZ1bmN0aW9uICgpIHtcblx0XHRhcHAuJGMud2luZG93Lm9uKCdsb2FkJywgYXBwLmFkZERvd25BcnJvdyk7XG5cdFx0YXBwLiRjLnN1Yk1lbnVQYXJlbnRJdGVtLm9uKCdjbGljaycsIGFwcC50b2dnbGVTdWJtZW51KTtcblx0XHRhcHAuJGMuc3ViTWVudVBhcmVudEl0ZW0ub24oJ3RyYW5zaXRpb25lbmQnLCBhcHAucmVzZXRTdWJNZW51KTtcblx0XHRhcHAuJGMub2ZmQ2FudmFzQ29udGFpbmVyLm9uKCd0cmFuc2l0aW9uZW5kJywgYXBwLmZvcmNlQ2xvc2VTdWJtZW51cyk7XG5cdH07XG5cblx0Ly8gRG8gd2UgbWVldCB0aGUgcmVxdWlyZW1lbnRzP1xuXHRhcHAubWVldHNSZXF1aXJlbWVudHMgPSBmdW5jdGlvbiAoKSB7XG5cdFx0cmV0dXJuIGFwcC4kYy5zdWJNZW51Q29udGFpbmVyLmxlbmd0aDtcblx0fTtcblxuXHQvLyBSZXNldCB0aGUgc3VibWVudXMgYWZ0ZXIgaXQncyBkb25lIGNsb3NpbmcuXG5cdGFwcC5yZXNldFN1Yk1lbnUgPSBmdW5jdGlvbiAoKSB7XG5cblx0XHQvLyBXaGVuIHRoZSBsaXN0IGl0ZW0gaXMgZG9uZSB0cmFuc2l0aW9uaW5nIGluIGhlaWdodCxcblx0XHQvLyByZW1vdmUgdGhlIGNsYXNzZXMgZnJvbSB0aGUgc3VibWVudSBzbyBpdCBpcyByZWFkeSB0byB0b2dnbGUgYWdhaW4uXG5cdFx0aWYgKCQodGhpcykuaXMoJ2xpLm1lbnUtaXRlbS1oYXMtY2hpbGRyZW4nKSAmJiAhJCh0aGlzKS5oYXNDbGFzcygnaXMtdmlzaWJsZScpKSB7XG5cdFx0XHQkKHRoaXMpLmZpbmQoJ3VsLnN1Yi1tZW51JykucmVtb3ZlQ2xhc3MoJ3NsaWRlT3V0TGVmdCBpcy12aXNpYmxlJyk7XG5cdFx0fVxuXG5cdH07XG5cblx0Ly8gU2xpZGUgb3V0IHRoZSBzdWJtZW51IGl0ZW1zLlxuXHRhcHAuc2xpZGVPdXRTdWJNZW51cyA9IGZ1bmN0aW9uIChlbCkge1xuXG5cdFx0Ly8gSWYgdGhpcyBpdGVtJ3MgcGFyZW50IGlzIHZpc2libGUgYW5kIHRoaXMgaXMgbm90LCBiYWlsLlxuXHRcdGlmIChlbC5wYXJlbnQoKS5oYXNDbGFzcygnaXMtdmlzaWJsZScpICYmICFlbC5oYXNDbGFzcygnaXMtdmlzaWJsZScpKSB7XG5cdFx0XHRyZXR1cm47XG5cdFx0fVxuXG5cdFx0Ly8gSWYgdGhpcyBpdGVtJ3MgcGFyZW50IGlzIHZpc2libGUgYW5kIHRoaXMgaXRlbSBpcyB2aXNpYmxlLCBoaWRlIGl0cyBzdWJtZW51IHRoZW4gYmFpbC5cblx0XHRpZiAoZWwucGFyZW50KCkuaGFzQ2xhc3MoJ2lzLXZpc2libGUnKSAmJiBlbC5oYXNDbGFzcygnaXMtdmlzaWJsZScpKSB7XG5cdFx0XHRlbC5yZW1vdmVDbGFzcygnaXMtdmlzaWJsZScpLmZpbmQoJy5zdWItbWVudScpLnJlbW92ZUNsYXNzKCdzbGlkZUluTGVmdCcpLmFkZENsYXNzKCdzbGlkZU91dExlZnQnKTtcblx0XHRcdHJldHVybjtcblx0XHR9XG5cblx0XHRhcHAuJGMuc3ViTWVudUNvbnRhaW5lci5lYWNoKGZ1bmN0aW9uICgpIHtcblxuXHRcdFx0Ly8gT25seSB0cnkgdG8gY2xvc2Ugc3VibWVudXMgdGhhdCBhcmUgYWN0dWFsbHkgb3Blbi5cblx0XHRcdGlmICgkKHRoaXMpLmhhc0NsYXNzKCdzbGlkZUluTGVmdCcpKSB7XG5cblx0XHRcdFx0Ly8gQ2xvc2UgdGhlIHBhcmVudCBsaXN0IGl0ZW0sIGFuZCBzZXQgdGhlIGNvcnJlc3BvbmRpbmcgYnV0dG9uIGFyaWEgdG8gZmFsc2UuXG5cdFx0XHRcdCQodGhpcykucGFyZW50KCkucmVtb3ZlQ2xhc3MoJ2lzLXZpc2libGUnKS5maW5kKCcucGFyZW50LWluZGljYXRvcicpLmF0dHIoJ2FyaWEtZXhwYW5kZWQnLCBmYWxzZSk7XG5cblx0XHRcdFx0Ly8gU2xpZGUgb3V0IHRoZSBzdWJtZW51LlxuXHRcdFx0XHQkKHRoaXMpLnJlbW92ZUNsYXNzKCdzbGlkZUluTGVmdCcpLmFkZENsYXNzKCdzbGlkZU91dExlZnQnKTtcblx0XHRcdH1cblxuXHRcdH0pO1xuXHR9O1xuXG5cdC8vIEFkZCB0aGUgZG93biBhcnJvdyB0byBzdWJtZW51IHBhcmVudHMuXG5cdGFwcC5hZGREb3duQXJyb3cgPSBmdW5jdGlvbiAoKSB7XG5cdFx0YXBwLiRjLnN1Yk1lbnVQYXJlbnRJdGVtLnByZXBlbmQoJzxidXR0b24gdHlwZT1cImJ1dHRvblwiIGFyaWEtZXhwYW5kZWQ9XCJmYWxzZVwiIGNsYXNzPVwicGFyZW50LWluZGljYXRvclwiIGFyaWEtbGFiZWw9XCJPcGVuIHN1Ym1lbnVcIj48c3BhbiBjbGFzcz1cImRvd24tYXJyb3dcIj48L3NwYW4+PC9idXR0b24+Jyk7XG5cdH07XG5cblx0Ly8gRGVhbCB3aXRoIHRoZSBzdWJtZW51LlxuXHRhcHAudG9nZ2xlU3VibWVudSA9IGZ1bmN0aW9uIChlKSB7XG5cblx0XHRsZXQgZWwgPSAkKHRoaXMpLCAvLyBUaGUgbWVudSBlbGVtZW50IHdoaWNoIHdhcyBjbGlja2VkIG9uLlxuXHRcdFx0c3ViTWVudSA9IGVsLmNoaWxkcmVuKCd1bC5zdWItbWVudScpLCAvLyBUaGUgbmVhcmVzdCBzdWJtZW51LlxuXHRcdFx0JHRhcmdldCA9ICQoZS50YXJnZXQpOyAvLyB0aGUgZWxlbWVudCB0aGF0J3MgYWN0dWFsbHkgYmVpbmcgY2xpY2tlZCAoY2hpbGQgb2YgdGhlIGxpIHRoYXQgdHJpZ2dlcmVkIHRoZSBjbGljayBldmVudCkuXG5cblx0XHQvLyBGaWd1cmUgb3V0IGlmIHdlJ3JlIGNsaWNraW5nIHRoZSBidXR0b24gb3IgaXRzIGFycm93IGNoaWxkLFxuXHRcdC8vIGlmIHNvLCB3ZSBjYW4ganVzdCBvcGVuIG9yIGNsb3NlIHRoZSBtZW51IGFuZCBiYWlsLlxuXHRcdGlmICgkdGFyZ2V0Lmhhc0NsYXNzKCdkb3duLWFycm93JykgfHwgJHRhcmdldC5oYXNDbGFzcygncGFyZW50LWluZGljYXRvcicpKSB7XG5cblx0XHRcdC8vIEZpcnN0LCBjb2xsYXBzZSBhbnkgYWxyZWFkeSBvcGVuZWQgc3VibWVudXMuXG5cdFx0XHRhcHAuc2xpZGVPdXRTdWJNZW51cyhlbCk7XG5cblx0XHRcdGlmICghc3ViTWVudS5oYXNDbGFzcygnaXMtdmlzaWJsZScpKSB7XG5cblx0XHRcdFx0Ly8gT3BlbiB0aGUgc3VibWVudS5cblx0XHRcdFx0YXBwLm9wZW5TdWJtZW51KGVsLCBzdWJNZW51KTtcblxuXHRcdFx0fVxuXG5cdFx0XHRyZXR1cm4gZmFsc2U7XG5cdFx0fVxuXG5cdH07XG5cblx0Ly8gT3BlbiBhIHN1Ym1lbnUuXG5cdGFwcC5vcGVuU3VibWVudSA9IGZ1bmN0aW9uIChwYXJlbnQsIHN1Yk1lbnUpIHtcblxuXHRcdC8vIEV4cGFuZCB0aGUgbGlzdCBtZW51IGl0ZW0sIGFuZCBzZXQgdGhlIGNvcnJlc3BvbmRpbmcgYnV0dG9uIGFyaWEgdG8gdHJ1ZS5cblx0XHRwYXJlbnQuYWRkQ2xhc3MoJ2lzLXZpc2libGUnKS5maW5kKCcucGFyZW50LWluZGljYXRvcicpLmF0dHIoJ2FyaWEtZXhwYW5kZWQnLCB0cnVlKTtcblxuXHRcdC8vIFNsaWRlIHRoZSBtZW51IGluLlxuXHRcdHN1Yk1lbnUuYWRkQ2xhc3MoJ2lzLXZpc2libGUgYW5pbWF0ZWQgc2xpZGVJbkxlZnQnKTtcblx0fTtcblxuXHQvLyBGb3JjZSBjbG9zZSBhbGwgdGhlIHN1Ym1lbnVzIHdoZW4gdGhlIG1haW4gbWVudSBjb250YWluZXIgaXMgY2xvc2VkLlxuXHRhcHAuZm9yY2VDbG9zZVN1Ym1lbnVzID0gZnVuY3Rpb24gKCkge1xuXG5cdFx0Ly8gVGhlIHRyYW5zaXRpb25lbmQgZXZlbnQgdHJpZ2dlcnMgb24gb3BlbiBhbmQgb24gY2xvc2UsIG5lZWQgdG8gbWFrZSBzdXJlIHdlIG9ubHkgZG8gdGhpcyBvbiBjbG9zZS5cblx0XHRpZiAoISQodGhpcykuaGFzQ2xhc3MoJ2lzLXZpc2libGUnKSkge1xuXHRcdFx0YXBwLiRjLnN1Yk1lbnVQYXJlbnRJdGVtLnJlbW92ZUNsYXNzKCdpcy12aXNpYmxlJykuZmluZCgnLnBhcmVudC1pbmRpY2F0b3InKS5hdHRyKCdhcmlhLWV4cGFuZGVkJywgZmFsc2UpO1xuXHRcdFx0YXBwLiRjLnN1Yk1lbnVDb250YWluZXIucmVtb3ZlQ2xhc3MoJ2lzLXZpc2libGUgc2xpZGVJbkxlZnQnKTtcblx0XHRcdGFwcC4kYy5ib2R5LmNzcygnb3ZlcmZsb3cnLCAndmlzaWJsZScpO1xuXHRcdFx0YXBwLiRjLmJvZHkudW5iaW5kKCd0b3VjaHN0YXJ0Jyk7XG5cdFx0fVxuXG5cdFx0aWYgKCQodGhpcykuaGFzQ2xhc3MoJ2lzLXZpc2libGUnKSkge1xuXHRcdFx0YXBwLiRjLmJvZHkuY3NzKCdvdmVyZmxvdycsICdoaWRkZW4nKTtcblx0XHRcdGFwcC4kYy5ib2R5LmJpbmQoJ3RvdWNoc3RhcnQnLCBmdW5jdGlvbiAoZSkge1xuXHRcdFx0XHRpZiAoISQoZS50YXJnZXQpLnBhcmVudHMoJy5jb250YWN0LW1vZGFsJylbMF0pIHtcblx0XHRcdFx0XHRlLnByZXZlbnREZWZhdWx0KCk7XG5cdFx0XHRcdH1cblx0XHRcdH0pO1xuXHRcdH1cblx0fTtcblxuXHQvLyBFbmdhZ2UhXG5cdCQoYXBwLmluaXQpO1xuXG59KHdpbmRvdywgalF1ZXJ5LCB3aW5kb3cud2RzTW9iaWxlTWVudSkpOyIsIi8qKlxuICogRmlsZSBtb2RhbC5qc1xuICpcbiAqIERlYWwgd2l0aCBtdWx0aXBsZSBtb2RhbHMgYW5kIHRoZWlyIG1lZGlhLlxuICovXG53aW5kb3cud2RzTW9kYWwgPSB7fTtcbihmdW5jdGlvbiAod2luZG93LCAkLCBhcHApIHtcblxuXHRsZXQgJG1vZGFsVG9nZ2xlLFxuXHRcdCRmb2N1c2FibGVDaGlsZHJlbixcblx0XHQkcGxheWVyLFxuXHRcdCR0YWcgPSBkb2N1bWVudC5jcmVhdGVFbGVtZW50KCdzY3JpcHQnKSxcblx0XHQkZmlyc3RTY3JpcHRUYWcgPSBkb2N1bWVudC5nZXRFbGVtZW50c0J5VGFnTmFtZSgnc2NyaXB0JylbMF0sXG5cdFx0WVQ7XG5cblx0Ly8gQ29uc3RydWN0b3IuXG5cdGFwcC5pbml0ID0gZnVuY3Rpb24gKCkge1xuXHRcdGFwcC5jYWNoZSgpO1xuXG5cdFx0aWYgKGFwcC5tZWV0c1JlcXVpcmVtZW50cygpKSB7XG5cdFx0XHQkZmlyc3RTY3JpcHRUYWcucGFyZW50Tm9kZS5pbnNlcnRCZWZvcmUoJHRhZywgJGZpcnN0U2NyaXB0VGFnKTtcblx0XHRcdGFwcC5iaW5kRXZlbnRzKCk7XG5cdFx0fVxuXHR9O1xuXG5cdC8vIENhY2hlIGFsbCB0aGUgdGhpbmdzLlxuXHRhcHAuY2FjaGUgPSBmdW5jdGlvbiAoKSB7XG5cdFx0YXBwLiRjID0ge1xuXHRcdFx0J2JvZHknOiAkKCdib2R5Jylcblx0XHR9O1xuXHR9O1xuXG5cdC8vIERvIHdlIG1lZXQgdGhlIHJlcXVpcmVtZW50cz9cblx0YXBwLm1lZXRzUmVxdWlyZW1lbnRzID0gZnVuY3Rpb24gKCkge1xuXHRcdHJldHVybiAkKCcubW9kYWwtdHJpZ2dlcicpLmxlbmd0aDtcblx0fTtcblxuXHQvLyBDb21iaW5lIGFsbCBldmVudHMuXG5cdGFwcC5iaW5kRXZlbnRzID0gZnVuY3Rpb24gKCkge1xuXG5cdFx0Ly8gVHJpZ2dlciBhIG1vZGFsIHRvIG9wZW4uXG5cdFx0YXBwLiRjLmJvZHkub24oJ2NsaWNrIHRvdWNoc3RhcnQnLCAnLm1vZGFsLXRyaWdnZXInLCBhcHAub3Blbk1vZGFsKTtcblxuXHRcdC8vIFRyaWdnZXIgdGhlIGNsb3NlIGJ1dHRvbiB0byBjbG9zZSB0aGUgbW9kYWwuXG5cdFx0YXBwLiRjLmJvZHkub24oJ2NsaWNrIHRvdWNoc3RhcnQnLCAnLmNsb3NlJywgYXBwLmNsb3NlTW9kYWwpO1xuXG5cdFx0Ly8gQWxsb3cgdGhlIHVzZXIgdG8gY2xvc2UgdGhlIG1vZGFsIGJ5IGhpdHRpbmcgdGhlIGVzYyBrZXkuXG5cdFx0YXBwLiRjLmJvZHkub24oJ2tleWRvd24nLCBhcHAuZXNjS2V5Q2xvc2UpO1xuXG5cdFx0Ly8gQWxsb3cgdGhlIHVzZXIgdG8gY2xvc2UgdGhlIG1vZGFsIGJ5IGNsaWNraW5nIG91dHNpZGUgb2YgdGhlIG1vZGFsLlxuXHRcdGFwcC4kYy5ib2R5Lm9uKCdjbGljayB0b3VjaHN0YXJ0JywgJ2Rpdi5tb2RhbC1vcGVuJywgYXBwLmNsb3NlTW9kYWxCeUNsaWNrKTtcblxuXHRcdC8vIExpc3RlbiB0byB0YWJzLCB0cmFwIGtleWJvYXJkIGlmIHdlIG5lZWQgdG9cblx0XHRhcHAuJGMuYm9keS5vbigna2V5ZG93bicsIGFwcC50cmFwS2V5Ym9hcmRNYXliZSk7XG5cblx0fTtcblxuXHQvLyBPcGVuIHRoZSBtb2RhbC5cblx0YXBwLm9wZW5Nb2RhbCA9IGZ1bmN0aW9uICgpIHtcblxuXHRcdC8vIFN0b3JlIHRoZSBtb2RhbCB0b2dnbGUgZWxlbWVudFxuXHRcdCRtb2RhbFRvZ2dsZSA9ICQodGhpcyk7XG5cblx0XHQvLyBGaWd1cmUgb3V0IHdoaWNoIG1vZGFsIHdlJ3JlIG9wZW5pbmcgYW5kIHN0b3JlIHRoZSBvYmplY3QuXG5cdFx0bGV0ICRtb2RhbCA9ICQoJCh0aGlzKS5kYXRhKCd0YXJnZXQnKSk7XG5cblx0XHQvLyBEaXNwbGF5IHRoZSBtb2RhbC5cblx0XHQkbW9kYWwuYWRkQ2xhc3MoJ21vZGFsLW9wZW4nKTtcblxuXHRcdC8vIEFkZCBib2R5IGNsYXNzLlxuXHRcdGFwcC4kYy5ib2R5LmFkZENsYXNzKCdtb2RhbC1vcGVuJyk7XG5cblx0XHQvLyBGaW5kIHRoZSBmb2N1c2FibGUgY2hpbGRyZW4gb2YgdGhlIG1vZGFsLlxuXHRcdC8vIFRoaXMgbGlzdCBtYXkgYmUgaW5jb21wbGV0ZSwgcmVhbGx5IHdpc2ggalF1ZXJ5IGhhZCB0aGUgOmZvY3VzYWJsZSBwc2V1ZG8gbGlrZSBqUXVlcnkgVUkgZG9lcy5cblx0XHQvLyBGb3IgbW9yZSBhYm91dCA6aW5wdXQgc2VlOiBodHRwczovL2FwaS5qcXVlcnkuY29tL2lucHV0LXNlbGVjdG9yL1xuXHRcdCRmb2N1c2FibGVDaGlsZHJlbiA9ICRtb2RhbC5maW5kKCdhLCA6aW5wdXQsIFt0YWJpbmRleF0nKTtcblxuXHRcdC8vIElkZWFsbHksIHRoZXJlIGlzIGFsd2F5cyBvbmUgKHRoZSBjbG9zZSBidXR0b24pLCBidXQgeW91IG5ldmVyIGtub3cuXG5cdFx0aWYgKDAgPCAkZm9jdXNhYmxlQ2hpbGRyZW4ubGVuZ3RoKSB7XG5cblx0XHRcdC8vIFNoaWZ0IGZvY3VzIHRvIHRoZSBmaXJzdCBmb2N1c2FibGUgZWxlbWVudC5cblx0XHRcdCRmb2N1c2FibGVDaGlsZHJlblswXS5mb2N1cygpO1xuXHRcdH1cblxuXHR9O1xuXG5cdC8vIENsb3NlIHRoZSBtb2RhbC5cblx0YXBwLmNsb3NlTW9kYWwgPSBmdW5jdGlvbiAoKSB7XG5cblx0XHQvLyBGaWd1cmUgdGhlIG9wZW5lZCBtb2RhbCB3ZSdyZSBjbG9zaW5nIGFuZCBzdG9yZSB0aGUgb2JqZWN0LlxuXHRcdGxldCAkbW9kYWwgPSAkKCQoJ2Rpdi5tb2RhbC1vcGVuIC5jbG9zZScpLmRhdGEoJ3RhcmdldCcpKSxcblxuXHRcdFx0Ly8gRmluZCB0aGUgaWZyYW1lIGluIHRoZSAkbW9kYWwgb2JqZWN0LlxuXHRcdFx0JGlmcmFtZSA9ICRtb2RhbC5maW5kKCdpZnJhbWUnKTtcblxuXHRcdC8vIE9ubHkgZG8gdGhpcyBpZiB0aGVyZSBhcmUgYW55IGlmcmFtZXMuXG5cdFx0aWYgKCRpZnJhbWUubGVuZ3RoKSB7XG5cblx0XHRcdC8vIEdldCB0aGUgaWZyYW1lIHNyYyBVUkwuXG5cdFx0XHRsZXQgdXJsID0gJGlmcmFtZS5hdHRyKCdzcmMnKTtcblxuXHRcdFx0Ly8gUmVtb3ZpbmcvUmVhZGRpbmcgdGhlIFVSTCB3aWxsIGVmZmVjdGl2ZWx5IGJyZWFrIHRoZSBZb3VUdWJlIEFQSS5cblx0XHRcdC8vIFNvIGxldCdzIG5vdCBkbyB0aGF0IHdoZW4gdGhlIGlmcmFtZSBVUkwgY29udGFpbnMgdGhlIGVuYWJsZWpzYXBpIHBhcmFtZXRlci5cblx0XHRcdGlmICghdXJsLmluY2x1ZGVzKCdlbmFibGVqc2FwaT0xJykpIHtcblxuXHRcdFx0XHQvLyBSZW1vdmUgdGhlIHNvdXJjZSBVUkwsIHRoZW4gYWRkIGl0IGJhY2ssIHNvIHRoZSB2aWRlbyBjYW4gYmUgcGxheWVkIGFnYWluIGxhdGVyLlxuXHRcdFx0XHQkaWZyYW1lLmF0dHIoJ3NyYycsICcnKS5hdHRyKCdzcmMnLCB1cmwpO1xuXHRcdFx0fSBlbHNlIHtcblxuXHRcdFx0XHQvLyBVc2UgdGhlIFlvdVR1YmUgQVBJIHRvIHN0b3AgdGhlIHZpZGVvLlxuXHRcdFx0XHQkcGxheWVyLnN0b3BWaWRlbygpO1xuXHRcdFx0fVxuXHRcdH1cblxuXHRcdC8vIEZpbmFsbHksIGhpZGUgdGhlIG1vZGFsLlxuXHRcdCRtb2RhbC5yZW1vdmVDbGFzcygnbW9kYWwtb3BlbicpO1xuXG5cdFx0Ly8gUmVtb3ZlIHRoZSBib2R5IGNsYXNzLlxuXHRcdGFwcC4kYy5ib2R5LnJlbW92ZUNsYXNzKCdtb2RhbC1vcGVuJyk7XG5cblx0XHQvLyBSZXZlcnQgZm9jdXMgYmFjayB0byB0b2dnbGUgZWxlbWVudFxuXHRcdCRtb2RhbFRvZ2dsZS5mb2N1cygpO1xuXG5cdH07XG5cblx0Ly8gQ2xvc2UgaWYgXCJlc2NcIiBrZXkgaXMgcHJlc3NlZC5cblx0YXBwLmVzY0tleUNsb3NlID0gZnVuY3Rpb24gKGV2ZW50KSB7XG5cdFx0aWYgKDI3ID09PSBldmVudC5rZXlDb2RlKSB7XG5cdFx0XHRhcHAuY2xvc2VNb2RhbCgpO1xuXHRcdH1cblx0fTtcblxuXHQvLyBDbG9zZSBpZiB0aGUgdXNlciBjbGlja3Mgb3V0c2lkZSBvZiB0aGUgbW9kYWxcblx0YXBwLmNsb3NlTW9kYWxCeUNsaWNrID0gZnVuY3Rpb24gKGV2ZW50KSB7XG5cblx0XHQvLyBJZiB0aGUgcGFyZW50IGNvbnRhaW5lciBpcyBOT1QgdGhlIG1vZGFsIGRpYWxvZyBjb250YWluZXIsIGNsb3NlIHRoZSBtb2RhbFxuXHRcdGlmICghJChldmVudC50YXJnZXQpLnBhcmVudHMoJ2RpdicpLmhhc0NsYXNzKCdtb2RhbC1kaWFsb2cnKSkge1xuXHRcdFx0YXBwLmNsb3NlTW9kYWwoKTtcblx0XHR9XG5cdH07XG5cblx0Ly8gVHJhcCB0aGUga2V5Ym9hcmQgaW50byBhIG1vZGFsIHdoZW4gb25lIGlzIGFjdGl2ZS5cblx0YXBwLnRyYXBLZXlib2FyZE1heWJlID0gZnVuY3Rpb24gKGV2ZW50KSB7XG5cblx0XHQvLyBXZSBvbmx5IG5lZWQgdG8gZG8gc3R1ZmYgd2hlbiB0aGUgbW9kYWwgaXMgb3BlbiBhbmQgdGFiIGlzIHByZXNzZWQuXG5cdFx0aWYgKDkgPT09IGV2ZW50LndoaWNoICYmIDAgPCAkKCcubW9kYWwtb3BlbicpLmxlbmd0aCkge1xuXHRcdFx0bGV0ICRmb2N1c2VkID0gJCgnOmZvY3VzJyksXG5cdFx0XHRcdGZvY3VzSW5kZXggPSAkZm9jdXNhYmxlQ2hpbGRyZW4uaW5kZXgoJGZvY3VzZWQpO1xuXG5cdFx0XHRpZiAoMCA9PT0gZm9jdXNJbmRleCAmJiBldmVudC5zaGlmdEtleSkge1xuXG5cdFx0XHRcdC8vIElmIHRoaXMgaXMgdGhlIGZpcnN0IGZvY3VzYWJsZSBlbGVtZW50LCBhbmQgc2hpZnQgaXMgaGVsZCB3aGVuIHByZXNzaW5nIHRhYiwgZ28gYmFjayB0byBsYXN0IGZvY3VzYWJsZSBlbGVtZW50LlxuXHRcdFx0XHQkZm9jdXNhYmxlQ2hpbGRyZW5bJGZvY3VzYWJsZUNoaWxkcmVuLmxlbmd0aCAtIDFdLmZvY3VzKCk7XG5cdFx0XHRcdGV2ZW50LnByZXZlbnREZWZhdWx0KCk7XG5cdFx0XHR9IGVsc2UgaWYgKCFldmVudC5zaGlmdEtleSAmJiBmb2N1c0luZGV4ID09PSAkZm9jdXNhYmxlQ2hpbGRyZW4ubGVuZ3RoIC0gMSkge1xuXG5cdFx0XHRcdC8vIElmIHRoaXMgaXMgdGhlIGxhc3QgZm9jdXNhYmxlIGVsZW1lbnQsIGFuZCBzaGlmdCBpcyBub3QgaGVsZCwgZ28gYmFjayB0byB0aGUgZmlyc3QgZm9jdXNhYmxlIGVsZW1lbnQuXG5cdFx0XHRcdCRmb2N1c2FibGVDaGlsZHJlblswXS5mb2N1cygpO1xuXHRcdFx0XHRldmVudC5wcmV2ZW50RGVmYXVsdCgpO1xuXHRcdFx0fVxuXHRcdH1cblx0fTtcblxuXHQvLyBIb29rIGludG8gWW91VHViZSA8aWZyYW1lPi5cblx0YXBwLm9uWW91VHViZUlmcmFtZUFQSVJlYWR5ID0gZnVuY3Rpb24gKCkge1xuXHRcdGxldCAkbW9kYWwgPSAkKCdkaXYubW9kYWwnKSxcblx0XHRcdCRpZnJhbWVpZCA9ICRtb2RhbC5maW5kKCdpZnJhbWUnKS5hdHRyKCdpZCcpO1xuXG5cdFx0JHBsYXllciA9IG5ldyBZVC5QbGF5ZXIoJGlmcmFtZWlkLCB7XG5cdFx0XHRldmVudHM6IHtcblx0XHRcdFx0J29uUmVhZHknOiBhcHAub25QbGF5ZXJSZWFkeSxcblx0XHRcdFx0J29uU3RhdGVDaGFuZ2UnOiBhcHAub25QbGF5ZXJTdGF0ZUNoYW5nZVxuXHRcdFx0fVxuXHRcdH0pO1xuXHR9O1xuXG5cdC8vIERvIHNvbWV0aGluZyBvbiBwbGF5ZXIgcmVhZHkuXG5cdGFwcC5vblBsYXllclJlYWR5ID0gZnVuY3Rpb24gKCkge307XG5cblx0Ly8gRG8gc29tZXRoaW5nIG9uIHBsYXllciBzdGF0ZSBjaGFuZ2UuXG5cdGFwcC5vblBsYXllclN0YXRlQ2hhbmdlID0gZnVuY3Rpb24gKCkge1xuXG5cdFx0Ly8gU2V0IGZvY3VzIHRvIHRoZSBmaXJzdCBmb2N1c2FibGUgZWxlbWVudCBpbnNpZGUgb2YgdGhlIG1vZGFsIHRoZSBwbGF5ZXIgaXMgaW4uXG5cdFx0JChldmVudC50YXJnZXQuYSkucGFyZW50cygnLm1vZGFsJykuZmluZCgnYSwgOmlucHV0LCBbdGFiaW5kZXhdJykuZmlyc3QoKS5mb2N1cygpO1xuXHR9O1xuXG5cblx0Ly8gRW5nYWdlIVxuXHQkKGFwcC5pbml0KTtcbn0od2luZG93LCBqUXVlcnksIHdpbmRvdy53ZHNNb2RhbCkpOyIsIi8qKlxuICogRmlsZTogbmF2aWdhdGlvbi1wcmltYXJ5LmpzXG4gKlxuICogSGVscGVycyBmb3IgdGhlIHByaW1hcnkgbmF2aWdhdGlvbi5cbiAqL1xud2luZG93Lndkc1ByaW1hcnlOYXZpZ2F0aW9uID0ge307XG4oZnVuY3Rpb24gKHdpbmRvdywgJCwgYXBwKSB7XG5cblx0Ly8gQ29uc3RydWN0b3IuXG5cdGFwcC5pbml0ID0gZnVuY3Rpb24gKCkge1xuXHRcdGFwcC5jYWNoZSgpO1xuXG5cdFx0aWYgKGFwcC5tZWV0c1JlcXVpcmVtZW50cygpKSB7XG5cdFx0XHRhcHAuYmluZEV2ZW50cygpO1xuXHRcdH1cblx0fTtcblxuXHQvLyBDYWNoZSBhbGwgdGhlIHRoaW5ncy5cblx0YXBwLmNhY2hlID0gZnVuY3Rpb24gKCkge1xuXHRcdGFwcC4kYyA9IHtcblx0XHRcdHdpbmRvdzogJCh3aW5kb3cpLFxuXHRcdFx0c3ViTWVudUNvbnRhaW5lcjogJCgnLm1haW4tbmF2aWdhdGlvbiAuc3ViLW1lbnUnKSxcblx0XHRcdHN1Yk1lbnVQYXJlbnRJdGVtOiAkKCcubWFpbi1uYXZpZ2F0aW9uIGxpLm1lbnUtaXRlbS1oYXMtY2hpbGRyZW4nKVxuXHRcdH07XG5cdH07XG5cblx0Ly8gQ29tYmluZSBhbGwgZXZlbnRzLlxuXHRhcHAuYmluZEV2ZW50cyA9IGZ1bmN0aW9uICgpIHtcblx0XHRhcHAuJGMud2luZG93Lm9uKCdsb2FkJywgYXBwLmFkZERvd25BcnJvdyk7XG5cdFx0YXBwLiRjLnN1Yk1lbnVQYXJlbnRJdGVtLmZpbmQoJ2EnKS5vbignZm9jdXNpbiBmb2N1c291dCcsIGFwcC50b2dnbGVGb2N1cyk7XG5cdH07XG5cblx0Ly8gRG8gd2UgbWVldCB0aGUgcmVxdWlyZW1lbnRzP1xuXHRhcHAubWVldHNSZXF1aXJlbWVudHMgPSBmdW5jdGlvbiAoKSB7XG5cdFx0cmV0dXJuIGFwcC4kYy5zdWJNZW51Q29udGFpbmVyLmxlbmd0aDtcblx0fTtcblxuXHQvLyBBZGQgdGhlIGRvd24gYXJyb3cgdG8gc3VibWVudSBwYXJlbnRzLlxuXHRhcHAuYWRkRG93bkFycm93ID0gZnVuY3Rpb24gKCkge1xuXHRcdGFwcC4kYy5zdWJNZW51UGFyZW50SXRlbS5maW5kKCc+IGEnKS5hcHBlbmQoJzxzcGFuIGNsYXNzPVwiY2FyZXQtZG93blwiIGFyaWEtaGlkZGVuPVwidHJ1ZVwiPjwvc3Bhbj4nKTtcblx0fTtcblxuXHQvLyBUb2dnbGUgdGhlIGZvY3VzIGNsYXNzIG9uIHRoZSBsaW5rIHBhcmVudC5cblx0YXBwLnRvZ2dsZUZvY3VzID0gZnVuY3Rpb24gKCkge1xuXHRcdCQodGhpcykucGFyZW50cygnbGkubWVudS1pdGVtLWhhcy1jaGlsZHJlbicpLnRvZ2dsZUNsYXNzKCdmb2N1cycpO1xuXHR9O1xuXG5cdC8vIEVuZ2FnZSFcblx0JChhcHAuaW5pdCk7XG5cbn0od2luZG93LCBqUXVlcnksIHdpbmRvdy53ZHNQcmltYXJ5TmF2aWdhdGlvbikpOyIsIi8qKlxuICogRmlsZTogb2ZmLWNhbnZhcy5qc1xuICpcbiAqIEhlbHAgZGVhbCB3aXRoIHRoZSBvZmYtY2FudmFzIG1vYmlsZSBtZW51LlxuICovXG53aW5kb3cud2Rzb2ZmQ2FudmFzID0ge307XG4oZnVuY3Rpb24gKHdpbmRvdywgJCwgYXBwKSB7XG5cblx0Ly8gQ29uc3RydWN0b3IuXG5cdGFwcC5pbml0ID0gZnVuY3Rpb24gKCkge1xuXHRcdGFwcC5jYWNoZSgpO1xuXG5cdFx0aWYgKGFwcC5tZWV0c1JlcXVpcmVtZW50cygpKSB7XG5cdFx0XHRhcHAuYmluZEV2ZW50cygpO1xuXHRcdH1cblx0fTtcblxuXHQvLyBDYWNoZSBhbGwgdGhlIHRoaW5ncy5cblx0YXBwLmNhY2hlID0gZnVuY3Rpb24gKCkge1xuXHRcdGFwcC4kYyA9IHtcblx0XHRcdGJvZHk6ICQoJ2JvZHknKSxcblx0XHRcdG9mZkNhbnZhc0Nsb3NlOiAkKCcub2ZmLWNhbnZhcy1jbG9zZScpLFxuXHRcdFx0b2ZmQ2FudmFzQ29udGFpbmVyOiAkKCcub2ZmLWNhbnZhcy1jb250YWluZXInKSxcblx0XHRcdG9mZkNhbnZhc09wZW46ICQoJy5vZmYtY2FudmFzLW9wZW4nKSxcblx0XHRcdG9mZkNhbnZhc1NjcmVlbjogJCgnLm9mZi1jYW52YXMtc2NyZWVuJylcblx0XHR9O1xuXHR9O1xuXG5cdC8vIENvbWJpbmUgYWxsIGV2ZW50cy5cblx0YXBwLmJpbmRFdmVudHMgPSBmdW5jdGlvbiAoKSB7XG5cdFx0YXBwLiRjLmJvZHkub24oJ2tleWRvd24nLCBhcHAuZXNjS2V5Q2xvc2UpO1xuXHRcdGFwcC4kYy5vZmZDYW52YXNDbG9zZS5vbignY2xpY2snLCBhcHAuY2xvc2VvZmZDYW52YXMpO1xuXHRcdGFwcC4kYy5vZmZDYW52YXNPcGVuLm9uKCdjbGljaycsIGFwcC50b2dnbGVvZmZDYW52YXMpO1xuXHRcdGFwcC4kYy5vZmZDYW52YXNTY3JlZW4ub24oJ2NsaWNrJywgYXBwLmNsb3Nlb2ZmQ2FudmFzKTtcblx0fTtcblxuXHQvLyBEbyB3ZSBtZWV0IHRoZSByZXF1aXJlbWVudHM/XG5cdGFwcC5tZWV0c1JlcXVpcmVtZW50cyA9IGZ1bmN0aW9uICgpIHtcblx0XHRyZXR1cm4gYXBwLiRjLm9mZkNhbnZhc0NvbnRhaW5lci5sZW5ndGg7XG5cdH07XG5cblx0Ly8gVG8gc2hvdyBvciBub3QgdG8gc2hvdz9cblx0YXBwLnRvZ2dsZW9mZkNhbnZhcyA9IGZ1bmN0aW9uICgpIHtcblxuXHRcdGlmICgndHJ1ZScgPT09ICQodGhpcykuYXR0cignYXJpYS1leHBhbmRlZCcpKSB7XG5cdFx0XHRhcHAuY2xvc2VvZmZDYW52YXMoKTtcblx0XHR9IGVsc2Uge1xuXHRcdFx0YXBwLm9wZW5vZmZDYW52YXMoKTtcblx0XHR9XG5cblx0fTtcblxuXHQvLyBTaG93IHRoYXQgZHJhd2VyIVxuXHRhcHAub3Blbm9mZkNhbnZhcyA9IGZ1bmN0aW9uICgpIHtcblx0XHRhcHAuJGMub2ZmQ2FudmFzQ29udGFpbmVyLmFkZENsYXNzKCdpcy12aXNpYmxlJyk7XG5cdFx0YXBwLiRjLm9mZkNhbnZhc09wZW4uYWRkQ2xhc3MoJ2lzLXZpc2libGUnKTtcblx0XHRhcHAuJGMub2ZmQ2FudmFzU2NyZWVuLmFkZENsYXNzKCdpcy12aXNpYmxlJyk7XG5cblx0XHRhcHAuJGMub2ZmQ2FudmFzT3Blbi5hdHRyKCdhcmlhLWV4cGFuZGVkJywgdHJ1ZSk7XG5cdFx0YXBwLiRjLm9mZkNhbnZhc0NvbnRhaW5lci5hdHRyKCdhcmlhLWhpZGRlbicsIGZhbHNlKTtcblxuXHRcdGFwcC4kYy5vZmZDYW52YXNDb250YWluZXIuZmluZCgnYnV0dG9uJykuZmlyc3QoKS5mb2N1cygpO1xuXHR9O1xuXG5cdC8vIENsb3NlIHRoYXQgZHJhd2VyIVxuXHRhcHAuY2xvc2VvZmZDYW52YXMgPSBmdW5jdGlvbiAoKSB7XG5cdFx0YXBwLiRjLm9mZkNhbnZhc0NvbnRhaW5lci5yZW1vdmVDbGFzcygnaXMtdmlzaWJsZScpO1xuXHRcdGFwcC4kYy5vZmZDYW52YXNPcGVuLnJlbW92ZUNsYXNzKCdpcy12aXNpYmxlJyk7XG5cdFx0YXBwLiRjLm9mZkNhbnZhc1NjcmVlbi5yZW1vdmVDbGFzcygnaXMtdmlzaWJsZScpO1xuXG5cdFx0YXBwLiRjLm9mZkNhbnZhc09wZW4uYXR0cignYXJpYS1leHBhbmRlZCcsIGZhbHNlKTtcblx0XHRhcHAuJGMub2ZmQ2FudmFzQ29udGFpbmVyLmF0dHIoJ2FyaWEtaGlkZGVuJywgdHJ1ZSk7XG5cblx0XHRhcHAuJGMub2ZmQ2FudmFzT3Blbi5mb2N1cygpO1xuXHR9O1xuXG5cdC8vIENsb3NlIGRyYXdlciBpZiBcImVzY1wiIGtleSBpcyBwcmVzc2VkLlxuXHRhcHAuZXNjS2V5Q2xvc2UgPSBmdW5jdGlvbiAoZXZlbnQpIHtcblx0XHRpZiAoMjcgPT09IGV2ZW50LmtleUNvZGUpIHtcblx0XHRcdGFwcC5jbG9zZW9mZkNhbnZhcygpO1xuXHRcdH1cblx0fTtcblxuXHQvLyBFbmdhZ2UhXG5cdCQoYXBwLmluaXQpO1xuXG59KHdpbmRvdywgalF1ZXJ5LCB3aW5kb3cud2Rzb2ZmQ2FudmFzKSk7IiwiLyoqXG4gKiBGaWxlIHNraXAtbGluay1mb2N1cy1maXguanMuXG4gKlxuICogSGVscHMgd2l0aCBhY2Nlc3NpYmlsaXR5IGZvciBrZXlib2FyZCBvbmx5IHVzZXJzLlxuICpcbiAqIExlYXJuIG1vcmU6IGh0dHBzOi8vZ2l0LmlvL3ZXZHIyXG4gKi9cbihmdW5jdGlvbiAoKSB7XG5cdHZhciBpc1dlYmtpdCA9IC0xIDwgbmF2aWdhdG9yLnVzZXJBZ2VudC50b0xvd2VyQ2FzZSgpLmluZGV4T2YoJ3dlYmtpdCcpLFxuXHRcdGlzT3BlcmEgPSAtMSA8IG5hdmlnYXRvci51c2VyQWdlbnQudG9Mb3dlckNhc2UoKS5pbmRleE9mKCdvcGVyYScpLFxuXHRcdGlzSWUgPSAtMSA8IG5hdmlnYXRvci51c2VyQWdlbnQudG9Mb3dlckNhc2UoKS5pbmRleE9mKCdtc2llJyk7XG5cblx0aWYgKChpc1dlYmtpdCB8fCBpc09wZXJhIHx8IGlzSWUpICYmIGRvY3VtZW50LmdldEVsZW1lbnRCeUlkICYmIHdpbmRvdy5hZGRFdmVudExpc3RlbmVyKSB7XG5cdFx0d2luZG93LmFkZEV2ZW50TGlzdGVuZXIoJ2hhc2hjaGFuZ2UnLCBmdW5jdGlvbiAoKSB7XG5cdFx0XHR2YXIgaWQgPSBsb2NhdGlvbi5oYXNoLnN1YnN0cmluZygxKSxcblx0XHRcdFx0ZWxlbWVudDtcblxuXHRcdFx0aWYgKCEoL15bQS16MC05Xy1dKyQvKS50ZXN0KGlkKSkge1xuXHRcdFx0XHRyZXR1cm47XG5cdFx0XHR9XG5cblx0XHRcdGVsZW1lbnQgPSBkb2N1bWVudC5nZXRFbGVtZW50QnlJZChpZCk7XG5cblx0XHRcdGlmIChlbGVtZW50KSB7XG5cdFx0XHRcdGlmICghKC9eKD86YXxzZWxlY3R8aW5wdXR8YnV0dG9ufHRleHRhcmVhKSQvaSkudGVzdChlbGVtZW50LnRhZ05hbWUpKSB7XG5cdFx0XHRcdFx0ZWxlbWVudC50YWJJbmRleCA9IC0xO1xuXHRcdFx0XHR9XG5cblx0XHRcdFx0ZWxlbWVudC5mb2N1cygpO1xuXHRcdFx0fVxuXHRcdH0sIGZhbHNlKTtcblx0fVxufSgpKTsiLCIvKipcbiAqIEZpbGUgd2luZG93LXJlYWR5LmpzXG4gKlxuICogQWRkIGEgXCJyZWFkeVwiIGNsYXNzIHRvIDxib2R5PiB3aGVuIHdpbmRvdyBpcyByZWFkeS5cbiAqL1xud2luZG93Lndkc1dpbmRvd1JlYWR5ID0ge307XG4oZnVuY3Rpb24gKHdpbmRvdywgJCwgYXBwKSB7XG5cblx0Ly8gQ29uc3RydWN0b3IuXG5cdGFwcC5pbml0ID0gZnVuY3Rpb24gKCkge1xuXHRcdGFwcC5jYWNoZSgpO1xuXHRcdGFwcC5iaW5kRXZlbnRzKCk7XG5cdH07XG5cblx0Ly8gQ2FjaGUgZG9jdW1lbnQgZWxlbWVudHMuXG5cdGFwcC5jYWNoZSA9IGZ1bmN0aW9uICgpIHtcblx0XHRhcHAuJGMgPSB7XG5cdFx0XHQnd2luZG93JzogJCh3aW5kb3cpLFxuXHRcdFx0J2JvZHknOiAkKGRvY3VtZW50LmJvZHkpXG5cdFx0fTtcblx0fTtcblxuXHQvLyBDb21iaW5lIGFsbCBldmVudHMuXG5cdGFwcC5iaW5kRXZlbnRzID0gZnVuY3Rpb24gKCkge1xuXHRcdGFwcC4kYy53aW5kb3cubG9hZChhcHAuYWRkQm9keUNsYXNzKTtcblx0fTtcblxuXHQvLyBBZGQgYSBjbGFzcyB0byA8Ym9keT4uXG5cdGFwcC5hZGRCb2R5Q2xhc3MgPSBmdW5jdGlvbiAoKSB7XG5cdFx0YXBwLiRjLmJvZHkuYWRkQ2xhc3MoJ3JlYWR5Jyk7XG5cdH07XG5cblx0Ly8gRW5nYWdlIVxuXHQkKGFwcC5pbml0KTtcbn0od2luZG93LCBqUXVlcnksIHdpbmRvdy53ZHNXaW5kb3dSZWFkeSkpOyJdfQ==
