<?php
/**
 * The template used for displaying X in the scaffolding library.
 *
 * @package Petey Greene
 */

?>

<section class="section-scaffolding">
	<h2 class="scaffolding-heading"><?php esc_html_e( 'X', 'petey-greene' ); ?></h2>
</section>
