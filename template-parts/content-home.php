<?php
/**
 * Template part for displaying posts.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package Petey Greene
 */
$article_url= get_field( 'news_link_source' );
?>
<article class="blog-post"<?php post_class();?>>
<?php
if (has_post_thumbnail()) {?>
	<figure class="featured-image index-image">
		<a href="<?php echo esc_url( $article_url ) /*esc_url(get_permalink())*/ ?>" rel="bookmark">
			<?php
the_post_thumbnail('blog_grid');
    ?>
		</a>
	</figure><!-- .featured-image full-bleed -->
	<?php }?>
<div class="entry-content">
<header class="entry-header">
	
		<?php
		if ( is_single() ) :
			the_title( '<h1 class="entry-title">', '</h1>' );
		else :
			//the_title( '<h2 class="entry-title"><a href="' . esc_url( $articleurl ) . '" rel="bookmark">', '</a></h2>' );
			the_title( '<h2 class="entry-title"><a href="' . esc_url( $article_url ) . '" rel="bookmark">', '</a></h2>' );
			
		endif;
		if ( 'post' === get_post_type() ) :
		?>
		
		<div class="entry-meta">
			<?php ptig_pgp_posted_on(); ?>
		</div><!-- .entry-meta -->
		<?php endif; ?>
	</header><!-- .entry-header -->

	
            <?php
             echo excerpt(30)
             ?>

	</div><!-- .entry-content -->
</article><!-- #post-## -->