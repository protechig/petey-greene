<?php
/**
 * The template used for displaying front video block.
 *
 * @package Petey Greene
 */

// Set up fields.
$title           = get_sub_field( 'title' );
$content         = get_sub_field( 'content' );
$animation_class = ptig_pgp_get_animation_class();
$button_url      = get_sub_field( 'button_link' );
$button_text     = get_sub_field( 'cta_button' );
// Start a <container> with possible block options.
ptig_pgp_display_block_options(
	array(
		'container' => 'section', // Any HTML5 container: section, div, etc...
		'class'     => 'content-block grid-container front-video', // Container class.
	)
);
?>
    <div class="grid-x <?php echo esc_attr( $animation_class ); ?>">
        <div class="front-content">
                <div class="video">
                    <div class="embed-container">
                        <?php the_sub_field('video'); ?>
                    </div>
                    <h3><?php the_sub_field('video_title'); ?></h3>
                </div>
                <div class="contents">
                    <h3><?php the_sub_field('content_header'); ?></h3>
                    <?php the_sub_field('description_content'); ?>
                </div>        
        </div>
        <div class="centerbtn">
             <?php if ( $button_url ) : ?>
				<button type="button" class="button button-hero" onclick="location.href='<?php echo esc_url( $button_url ); ?>'"><?php echo esc_html( $button_text ); ?></button>
            <?php endif; ?>
        </div>
	</div><!-- .grid-x -->
</section><!-- .generic-content -->
