<?php
/**
 * The template used for displaying centered video block.
 *
 * @package Petey Greene
 */

// Set up fields.
$animation_class = ptig_pgp_get_animation_class();
// Start a <container> with possible block options.
ptig_pgp_display_block_options(
	array(
		'container' => 'section', // Any HTML5 container: section, div, etc...
		'class'     => 'content-block grid-container centered-vid', // Container class.
	)
);
?>

    <div class="grid-x <?php echo esc_attr( $animation_class ); ?>">
    <div class="heading">
                <h2><?php the_sub_field('header'); ?></h2>  
    </div>
        <div class="centered-video">
                <div class="video">
                    <div class="embed-container">
                        <?php the_sub_field('video'); ?>
                    </div>
                </div>        
        </div>
	</div><!-- .grid-x -->
</section><!-- .generic-content -->
