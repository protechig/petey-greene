<?php
/**
 * The template used for displaying a hero.
 *
 * @package Petey Greene
 */

// Set up fields.
$hero        = get_sub_field( 'hero_slides' );
$slide_count = count( $hero );

// Start repeater markup...
if ( have_rows( 'hero_slides' ) ) :

	// If there is more than one slide...
	if ( $slide_count > 1 ) :
		echo '<section class="content-block container carousel">';

		// Enqueue Slick.
		wp_enqueue_style( 'slick-carousel' );
		wp_enqueue_script( 'slick-carousel' );
	endif;

	// Loop through hero(s).
	while ( have_rows( 'hero_slides' ) ) :
		the_row();

		// Set up fields.
		$title           = get_sub_field( 'headline' );
		$text            = get_sub_field( 'text' );
		$donate_text     = get_sub_field( 'donate_button_txt' );
		$donate_url      = get_sub_field( 'donate_url' );
		$volunteer_text     = get_sub_field( 'volunteer_button_txt' );
		$volunteer_url      = get_sub_field( 'volunteer_url' );
		$animation_class = ptig_pgp_get_animation_class();


		// Start a <container> with possible block options.
		ptig_pgp_display_block_options( array(
			'container' => 'section', // Any HTML5 container: section, div, etc...
			'class'     => 'content-block hero slide', // Container class.
		) );

		// If we have a slider, set the animation in a data-attribute.
		if ( $slide_count > 1 ) : ?>
			<div class="hero-content " data-animation="<?php echo esc_attr( $animation_class ); ?>">
		<?php else : ?>
			<div class="hero-content <?php echo esc_attr( $animation_class ); ?>">
		<?php endif; ?>

			<?php if ( $title ) : ?>
				<h3 class="hero-title"><?php echo esc_html( $title ); ?></h3>
			<?php endif; ?>

			<?php if ( $text ) : ?>
				<h2 class="hero-description"><?php echo esc_html( $text ); ?></h2>
			<?php endif; ?>
           <div class="cta-button">
			<?php if ( $donate_url ) : ?>
				<p><button type="button" class="button button-hero" onclick="location.href='<?php echo esc_url( $donate_url ); ?>'"><?php echo esc_html( $donate_text ); ?></button>
			<?php endif; ?>
			<span>OR</span>
			<?php if ( $volunteer_url ) : ?>
				<button type="button" class="button button-hero secondary-btn" onclick="location.href='<?php echo esc_url( $volunteer_url ); ?>'"><?php echo esc_html( $volunteer_text ); ?></button>
			<?php endif; ?>
		   </div>
		</div><!-- .hero-content -->
	</section><!-- .hero -->

<?php
	endwhile;

	if ( $slide_count > 1 ) :
		echo '</section>';
	endif;

endif;
?>
