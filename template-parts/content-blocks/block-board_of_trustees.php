
<?php
/**
 *  The template used for displaying fifty/fifty text/media.
 *
 * @package Petey Greene
 */

// Set up fields.
$animation_class = ptig_pgp_get_animation_class();

// Start a <container> with a possible media background.
ptig_pgp_display_block_options( array(
	'container' => 'section', // Any HTML5 container: section, div, etc...
	'class'     => 'content-block grid-container board-of-trustees', // Container class.
) );
?>
          <div class="grid-x">
            <h2 class="boar-header"><?php the_sub_field('board_section_titile'); ?></h2>
                
             <div class="member-grid">
                <?php if(have_rows('board_of_trustees')): ?>
                    <?php while(have_rows('board_of_trustees')) : the_row(); ?>
                        <div class="member-info">
                            <p><?php the_sub_field('names'); ?></p>
                         </div>
                    <?php endwhile; ?>
                <?php endif; ?>
                </div>
              </div>
            </section>