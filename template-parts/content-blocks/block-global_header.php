<?php
/**
 * The template used for displaying Global header block.
 *
 * @package Petey Greene
 */

// Set up fields.
$animation_class = ptig_pgp_get_animation_class();
$button_url      = get_sub_field( 'button_url' );
$button_text     = get_sub_field( 'button' );
// Start a <container> with possible block options.
ptig_pgp_display_block_options(
	array(
		'container' => 'section', // Any HTML5 container: section, div, etc...
		'class'     => 'content-block grid-container global-header', // Container class.
	)
);
?>
    <div class="grid-x <?php echo esc_attr( $animation_class ); ?>">
    <div class="header">
       <h2><?php the_sub_field('header'); ?></h2>
</div>
	</div><!-- .grid-x -->
</section><!-- .generic-content -->