<?php
/**
 * The template used for displaying a generic content block.
 *
 * @package Petey Greene
 */

// Set up fields.
$title           = get_sub_field( 'title' );
$content         = get_sub_field( 'content' );
$animation_class = ptig_pgp_get_animation_class();
$header          = get_sub_field('header');

// Start a <container> with possible block options.
ptig_pgp_display_block_options(
	array(
		'container' => 'section', // Any HTML5 container: section, div, etc...
		'class'     => 'content-block grid-container generic-content', // Container class.
	)
);
?>
	<div class="grid-x <?php echo esc_attr( $animation_class ); ?>">

		<?php if ( $title ) : ?>
			<h2 class="title"><?php echo esc_html( $title ); ?></h2>
		<?php endif; ?>
         <h2 class="generic-header">
		 <?php
		  echo esc_html( $header); 
		  ?>
		  </h2>
		<?php
			echo force_balance_tags( $content ); // WP XSS OK.
		?>

	</div><!-- .grid-x -->
</section><!-- .generic-content -->
