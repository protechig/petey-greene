<?php
/**
 * The template used for displaying learn more block.
 *
 * @package Petey Greene
 */

// Set up fields.
$animation_class = ptig_pgp_get_animation_class();
$button_url      = get_sub_field( 'button_url' );
$button_text     = get_sub_field( 'button' );
// Start a <container> with possible block options.
ptig_pgp_display_block_options(
	array(
		'container' => 'section', // Any HTML5 container: section, div, etc...
		'class'     => 'content-block grid-container learn-more', // Container class.
	)
);
?>
    <div class="grid-x <?php echo esc_attr( $animation_class ); ?>">
        <div class="lern-more-content">
            <div class="logo">
               <img src="<?php the_sub_field('logo'); ?>">
            </div>
            <div class="heading">
                <h3><?php the_sub_field('heading'); ?></h3>  
            </div>
            <div class="btn">
            <?php if ( $button_url ) : ?>
                      <button type="button" class="button primary-btn" onclick="location.href='<?php echo esc_url( $button_url ); ?>'"><?php echo esc_html( $button_text ); ?></button>
                 <?php endif; ?>
            </div>
        </div>
        
	</div><!-- .grid-x -->
</section><!-- .generic-content -->