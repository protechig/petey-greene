<?php
/**
 * The template used for displaying a Volunteer content block.
 *
 * @package Petey Greene
 */

// Set up fields.
$content         = get_sub_field( 'content_area' );
$animation_class = ptig_pgp_get_animation_class();
$header          = get_sub_field('header');
$button_text     = get_sub_field( 'button' );
$button_url      = get_sub_field( 'button_link' );

// Start a <container> with possible block options.
ptig_pgp_display_block_options(
	array(
		'container' => 'section', // Any HTML5 container: section, div, etc...
		'class'     => 'content-block grid-container volunteers', // Container class.
	)
);
?>
	<div class="grid-x <?php echo esc_attr( $animation_class ); ?>">

		
         <h2 class="heading">
		 <?php
		  echo esc_html( $header); 
		  ?>
          </h2>
          <div class="content-area">
		<?php
			echo force_balance_tags( $content ); // WP XSS OK.
		?>
       </div>
       <div class="cta-btn">
       <button type="button" class="button button-hero " onclick="location.href='<?php echo esc_url( $button_url ); ?>'"><?php echo esc_html( $button_text ); ?></button>
        </div>
	</div><!-- .grid-x -->
</section><!-- .generic-content -->
