<?php
/**
 * Template Name: donation page
 *
 * This template displays a page with a sidebar on the right side of the screen.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package Petey Greene
 */

get_header(); ?>

	<div class="primary content-area wrap main-top">
		<main id="main" class="site-main">

			<?php
			while ( have_posts() ) :
				the_post();

				get_template_part( 'template-parts/content', 'donation' );


			endwhile; // End of the loop.
			?>
  <div class="logo-grid">
                <?php if(have_rows('logos')): ?>
                    <?php while(have_rows('logos')) : the_row(); ?>
                        <div class="logo">
                           <a href="<?php the_sub_field('source_url'); ?>"><img class="image" src=<?php the_sub_field('image'); ?> /></a>
                         </div>
                    <?php endwhile; ?>
                <?php endif; ?>
                </div>
		</main><!-- #main -->
	</div><!-- .primary -->

<?php get_footer(); ?>