<?php
/**
 * Template Name: Sidebar Right
 *
 * This template displays a page with a sidebar on the right side of the screen.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package Petey Greene
 */

get_header(); ?>
<div class="content-area volunteer-programs">
		<main id="main" class="site-main">
		<?php
			// If the page is password protected...
			if ( post_password_required() ) :
				get_template_part( 'template-parts/content', 'password-protected' );
			else :
				ptig_pgp_display_content_blocks();
			endif;
		?>
		</main><!-- #main -->
	</div><!-- .primary -->
<?php get_sidebar('volunteer'); ?>

	<div class="primary content-area col-l-8 volunteer-content">
		<main id="main" class="site-main">

			<?php
			while ( have_posts() ) :
				the_post();

				get_template_part( 'template-parts/content', 'volunteer' );

			endwhile; // End of the loop.
			?>

		</main><!-- #main -->
	</div><!-- .primary -->

	


<?php get_footer(); ?>