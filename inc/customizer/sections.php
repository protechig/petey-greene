<?php
/**
 * Customizer sections.
 *
 * @package Petey Greene
 */

/**
 * Register the section sections.
 *
 * @param object $wp_customize Instance of WP_Customize_Class.
 */
function ptig_pgp_customize_sections( $wp_customize ) {

	// Register additional scripts section.
	$wp_customize->add_section(
		'ptig_pgp_additional_scripts_section',
		array(
			'title'    => esc_html__( 'Additional Scripts', 'petey-greene' ),
			'priority' => 10,
			'panel'    => 'site-options',
		)
	);

	// Register a social links section.
	$wp_customize->add_section(
		'ptig_pgp_social_links_section',
		array(
			'title'       => esc_html__( 'Social Media', 'petey-greene' ),
			'description' => esc_html__( 'Links here power the display_social_network_links() template tag.', 'petey-greene' ),
			'priority'    => 90,
			'panel'       => 'site-options',
		)
	);

	// Register a header section.
	$wp_customize->add_section(
		'ptig_pgp_header_section',
		array(
			'title'    => esc_html__( 'Header Customizations', 'petey-greene' ),
			'priority' => 90,
			'panel'    => 'site-options',
		)
	);

	// Register a footer section.
	$wp_customize->add_section(
		'ptig_pgp_footer_section',
		array(
			'title'    => esc_html__( 'Footer Customizations', 'petey-greene' ),
			'priority' => 90,
			'panel'    => 'site-options',
		)
	);
}
add_action( 'customize_register', 'ptig_pgp_customize_sections' );
