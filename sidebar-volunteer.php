<?php
/**
 * The sidebar containing the main widget area.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Petey Greene
 */

if ( ! is_active_sidebar( 'sidebar-volunteer' ) ) {
	return;
}
?>

<aside class="secondary widget-area col-l-4 volunteer" role="complementary">
	<?php dynamic_sidebar( 'sidebar-volunteer' ); ?>
</aside><!-- .secondary -->