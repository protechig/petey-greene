<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Petey Greene
 */

?>

	</div><!-- #content -->

	<footer class="site-footer">
<div class="grid-container content-block">
   <div class="grid-x">
     <div class="cell">
	 <div class="site-branding">
			<?php the_custom_logo(); ?>
			
			<div class="social-icons">
					<a href="<?php  the_field( 'facebook', 'option' ); ?>" target=_blank >
					<span class="fa-stack fa-lg">
					  <i class="fa fa-circle fa-stack-2x"></i>
					  <i class="fa fa-facebook fa-stack-1x fa-inverse"></i>
					</span></a>
					<a href="<?php  the_field( 'linkedin' , 'option'); ?>" target=_blank><span class="fa-stack fa-lg">
					  <i class="fa fa-circle fa-stack-2x"></i>
					  <i class="fa fa-linkedin fa-stack-1x fa-inverse"></i>
					</span></a>
					<a href="<?php  the_field('twitter', 'option'); ?>" target=_blank><span class="fa-stack fa-lg">
					  <i class="fa fa-circle fa-stack-2x"></i>
					  <i class="fa fa-twitter fa-stack-1x fa-inverse"></i>
					</span></a>
					<a href="<?php  the_field('instagram', 'option'); ?>" target=_blank><span class="fa-stack fa-lg">
					  <i class="fa fa-circle fa-stack-2x"></i>
					  <i class="fa fa-instagram fa-stack-1x fa-inverse"></i>
					</span></a>
             </div>
		</div><!-- .site-branding -->
	 	
	 </div>

     <div class="cell">
	 <h3><?php the_field('guide_star_gold_header','option'); ?></h3>
	 <div class="golden">
	 <a href="<?php the_field('profile_link', 'option'); ?>"><img class="fifty-image" src="<?php the_field('image', 'option'); ?>" alt="<?php the_field('image', 'option'); ?>"></a>
	 <a href="<?php the_field('profile_link', 'option'); ?>"><?php the_field('our_profile', 'option'); ?></a>
	 </div>
	 </div>
     <div class="cell">
	 <h3><?php the_field('financial_info_header', 'option'); ?></h3>
	 <?php if(have_rows('sources', 'option')): ?>
                    <?php while(have_rows('sources', 'option')) : the_row(); ?>
                            <a href="<?php the_sub_field('source_link', 'option'); ?>"><?php the_sub_field('source_name', 'option'); ?><br></a>
                    <?php endwhile; ?>
                <?php endif; ?>
	 <a > </a>
	 </div>
     <div class="cell">
	 <h3><?php the_field('our_info_header', 'option'); ?></h3>
	 <p><?php the_field('locations', 'option'); ?></p>
	 <a href="mailto:<?php the_field('email_link', 'option'); ?>"> <?php the_field('email_text', 'option'); ?></a>
	 <p><a href="<?php the_field('careers_link', 'option'); ?>"> <?php the_field('careers', 'option'); ?></a></P>
	 </div>
   </div>
  </div>
		

		<div class="site-info">
			<span class="copyright-text">
				<p>&copy; Copyright <?php echo date('Y'); ?> The Petey Greene Program. All Rights Reserved</p>
			</span>
			
		</div><!-- .site-info -->
	</footer><!-- .site-footer container-->
</div><!-- #page -->

<?php wp_footer(); ?>

<nav class="off-canvas-container" aria-hidden="true">
	<button type="button" class="off-canvas-close" aria-label="<?php esc_html_e( 'Close Menu', 'petey-greene' ); ?>">
		<span class="close"></span>
	</button>
	<?php
		// Mobile menu args.
		$mobile_args = array(
			'theme_location'  => 'mobile',
			'container'       => 'div',
			'container_class' => 'off-canvas-content',
			'container_id'    => '',
			'menu_id'         => 'mobile-menu',
			'menu_class'      => 'mobile-menu',
		);

		// Display the mobile menu.
		wp_nav_menu( $mobile_args );
	?>
</nav>
<div class="off-canvas-screen"></div>
</body>
</html>
